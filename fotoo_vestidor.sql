-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Oct 04, 2016 at 03:41 PM
-- Server version: 5.5.45-37.4-log
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `fotoo_vestidor`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog_categories`
--

CREATE TABLE IF NOT EXISTS `blog_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pos` int(11) NOT NULL DEFAULT '0',
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `blog_categories`
--

INSERT INTO `blog_categories` (`id`, `pos`, `title`) VALUES
(1, 1, 'Category 1'),
(2, 2, 'Category 2'),
(3, 3, 'Category 3');

-- --------------------------------------------------------

--
-- Table structure for table `blog_posts`
--

CREATE TABLE IF NOT EXISTS `blog_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL DEFAULT '1',
  `author_id` int(11) NOT NULL,
  `title` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `content_brief` text COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `publish_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('draft','active','hidden') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'draft',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `blog_posts`
--

INSERT INTO `blog_posts` (`id`, `category_id`, `author_id`, `title`, `content_brief`, `content`, `publish_time`, `status`) VALUES
(1, 1, 1, 'Blog Post 1', '<p>\r\n	Blog Post 1 Content Brief</p>\r\n', '<p>\r\n	Blog Post 1 Content</p>\r\n', '2015-09-26 00:00:00', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `blog_post_tag_rel`
--

CREATE TABLE IF NOT EXISTS `blog_post_tag_rel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `blog_post_tag_rel`
--

INSERT INTO `blog_post_tag_rel` (`id`, `post_id`, `tag_id`) VALUES
(1, 1, 2),
(2, 1, 1),
(3, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `blog_tags`
--

CREATE TABLE IF NOT EXISTS `blog_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `blog_tags`
--

INSERT INTO `blog_tags` (`id`, `title`) VALUES
(1, 'Tag 1'),
(2, 'Tag 2'),
(3, 'Tag 3');

-- --------------------------------------------------------

--
-- Table structure for table `cover_photos`
--

CREATE TABLE IF NOT EXISTS `cover_photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pos` int(11) NOT NULL DEFAULT '0',
  `image_url` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('active','hidden') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `cover_photos`
--

INSERT INTO `cover_photos` (`id`, `pos`, `image_url`, `status`) VALUES
(1, 2, '45296-2.jpg', 'active'),
(2, 1, '2934f-1.jpg', 'active'),
(3, 3, '3717d-3.jpg', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Super Admin'),
(2, 'manager', 'Admin'),
(3, 'staff', 'Stylist'),
(4, 'members', 'Customer');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_appointment`
--

CREATE TABLE IF NOT EXISTS `tb_appointment` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `stylistID` int(11) NOT NULL,
  `boxID` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `createdOn` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_box`
--

CREATE TABLE IF NOT EXISTS `tb_box` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `stylistID` int(11) NOT NULL,
  `createdOn` datetime NOT NULL,
  `updatedOn` datetime NOT NULL,
  `orderDate` date NOT NULL,
  `withdrawDate` date NOT NULL,
  `estimatedAmount` float NOT NULL,
  `boxStatus` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `tb_box`
--

INSERT INTO `tb_box` (`ID`, `userID`, `stylistID`, `createdOn`, `updatedOn`, `orderDate`, `withdrawDate`, `estimatedAmount`, `boxStatus`) VALUES
(6, 51, 6, '2016-09-30 10:38:26', '0000-00-00 00:00:00', '2016-10-03', '2016-10-08', 951, 1),
(7, 67, 7, '2016-09-30 03:52:51', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', 0, 0),
(9, 51, 6, '2016-10-03 01:29:38', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', 0, 0),
(10, 91, 7, '2016-10-04 11:59:57', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_box_product`
--

CREATE TABLE IF NOT EXISTS `tb_box_product` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `boxID` int(11) NOT NULL,
  `productID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `tb_box_product`
--

INSERT INTO `tb_box_product` (`ID`, `boxID`, `productID`) VALUES
(25, 6, 5),
(24, 6, 4),
(26, 8, 6),
(27, 8, 4),
(28, 8, 5);

-- --------------------------------------------------------

--
-- Table structure for table `tb_brand`
--

CREATE TABLE IF NOT EXISTS `tb_brand` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `brandName` varchar(100) NOT NULL,
  `adminID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tb_brand`
--

INSERT INTO `tb_brand` (`ID`, `brandName`, `adminID`) VALUES
(1, 'lee', 2),
(2, 'spykar', 2),
(5, 'puma', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tb_category`
--

CREATE TABLE IF NOT EXISTS `tb_category` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `categoryName` varchar(100) NOT NULL,
  `adminID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tb_category`
--

INSERT INTO `tb_category` (`ID`, `categoryName`, `adminID`) VALUES
(1, 'formal wear', 2),
(2, 'casual wear', 2),
(4, 'party wear', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tb_order`
--

CREATE TABLE IF NOT EXISTS `tb_order` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `amount` float NOT NULL,
  `dateTimeofOrder` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tb_order`
--

INSERT INTO `tb_order` (`ID`, `userID`, `amount`, `dateTimeofOrder`, `status`) VALUES
(3, 51, 938, '2016-10-03 07:21:09', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_orderproducts`
--

CREATE TABLE IF NOT EXISTS `tb_orderproducts` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `orderID` int(11) NOT NULL,
  `boxID` int(11) NOT NULL,
  `productID` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tb_orderproducts`
--

INSERT INTO `tb_orderproducts` (`ID`, `orderID`, `boxID`, `productID`, `quantity`) VALUES
(5, 3, 6, 5, 0),
(6, 3, 6, 4, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_products`
--

CREATE TABLE IF NOT EXISTS `tb_products` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `status` int(11) NOT NULL,
  `sku` varchar(50) NOT NULL,
  `brandName` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `shortDes` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `simplePrice` float NOT NULL,
  `discountPrice` float NOT NULL,
  `quantity` int(11) NOT NULL,
  `adminID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tb_products`
--

INSERT INTO `tb_products` (`ID`, `name`, `status`, `sku`, `brandName`, `category`, `shortDes`, `description`, `simplePrice`, `discountPrice`, `quantity`, `adminID`) VALUES
(2, 'formal shirts ', 0, '4564', 1, 1, 'cotton shirt blue color', 'amazing shirt', 13, 1, 20, 2),
(6, 'jeans', 1, '1020', 1, 4, 'blue jeans', 'amazing jeans', 20, 2, 2, 2),
(4, 'tshirt', 0, '10nm23', 5, 2, 'tshirt', 'tshirt good', 30, 10, 10, 2),
(5, 'casual shirt', 1, '1040', 2, 2, 'casual cotton shirt', '', 908, 100, 34, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tb_products_image`
--

CREATE TABLE IF NOT EXISTS `tb_products_image` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `productID` int(11) NOT NULL,
  `image` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tb_products_image`
--

INSERT INTO `tb_products_image` (`ID`, `productID`, `image`) VALUES
(4, 2, '7a02e-shirt.jpg'),
(3, 4, '66eed-tishirt.jpg'),
(5, 5, '68db4-casual-shirt.jpg'),
(7, 6, '32f16-jeance.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_settings`
--

CREATE TABLE IF NOT EXISTS `tb_settings` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `logo` varchar(50) NOT NULL,
  `copyrights` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_shipment`
--

CREATE TABLE IF NOT EXISTS `tb_shipment` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `boxID` int(11) NOT NULL,
  `addressType` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tb_shipment`
--

INSERT INTO `tb_shipment` (`ID`, `userID`, `boxID`, `addressType`, `status`) VALUES
(4, 51, 6, 1, 3),
(3, 51, 8, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_stylist_details`
--

CREATE TABLE IF NOT EXISTS `tb_stylist_details` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `phoneOne` varchar(50) NOT NULL,
  `phoneTwo` varchar(50) NOT NULL,
  `address` varchar(255) NOT NULL,
  `adminID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=58 ;

--
-- Dumping data for table `tb_stylist_details`
--

INSERT INTO `tb_stylist_details` (`ID`, `userID`, `phoneOne`, `phoneTwo`, `address`, `adminID`) VALUES
(1, 3, '987', '654321', 'fdfgfd', 2),
(56, 56, '123456789', '0123456789', 'abcabc', 2),
(6, 57, '15624', '5249656', 'dfvfddfg', 2),
(7, 58, '5241561', '54151', 'ffdgdgd', 2),
(57, 68, '1234567890', '', 'ahmedabad', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tb_traractions`
--

CREATE TABLE IF NOT EXISTS `tb_traractions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `orderID` int(11) NOT NULL,
  `traractionDateTime` datetime NOT NULL,
  `token` int(11) NOT NULL,
  `amountReceived` int(11) NOT NULL,
  `paymentMode` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user_details`
--

CREATE TABLE IF NOT EXISTS `tb_user_details` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `stylistID` int(11) NOT NULL,
  `username` varchar(25) NOT NULL,
  `surname` varchar(25) NOT NULL,
  `phone` varchar(25) NOT NULL,
  `dob` date NOT NULL,
  `gender` varchar(25) NOT NULL,
  `province` varchar(25) NOT NULL,
  `zipCode` int(11) NOT NULL,
  `city` varchar(25) NOT NULL,
  `height` float NOT NULL,
  `weight` float NOT NULL,
  `eyeColor` varchar(25) NOT NULL,
  `hairColor` varchar(25) NOT NULL,
  `shirtSize` float NOT NULL,
  `neck` float NOT NULL,
  `sleeveLength` float NOT NULL,
  `jacket` float NOT NULL,
  `shoes` float NOT NULL,
  `trousers` float NOT NULL,
  `inseam` float NOT NULL,
  `dress` float NOT NULL,
  `bust` float NOT NULL,
  `band` float NOT NULL,
  `cup` float NOT NULL,
  `profilePicture` varchar(100) NOT NULL,
  `photo` varchar(100) NOT NULL,
  `shipStreet` varchar(100) NOT NULL,
  `shipNumber` varchar(25) NOT NULL,
  `shipFloor` varchar(25) NOT NULL,
  `shipApartment` varchar(25) NOT NULL,
  `shipProvince` varchar(25) NOT NULL,
  `shipZipcode` int(11) NOT NULL,
  `shipCity` varchar(25) NOT NULL,
  `type` varchar(25) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=65 ;

--
-- Dumping data for table `tb_user_details`
--

INSERT INTO `tb_user_details` (`ID`, `userID`, `stylistID`, `username`, `surname`, `phone`, `dob`, `gender`, `province`, `zipCode`, `city`, `height`, `weight`, `eyeColor`, `hairColor`, `shirtSize`, `neck`, `sleeveLength`, `jacket`, `shoes`, `trousers`, `inseam`, `dress`, `bust`, `band`, `cup`, `profilePicture`, `photo`, `shipStreet`, `shipNumber`, `shipFloor`, `shipApartment`, `shipProvince`, `shipZipcode`, `shipCity`, `type`) VALUES
(64, 91, 7, 'rajnik', 'bambhaniya', '9978498512', '0000-00-00', 'Male', 'ttt', 888, 'abc', 6, 6, 'Black', 'Black', 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, '', '', 'uuuu', '777', 'iii', 'lll', 'ttt', 222, 'abc', 'Home');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `pass` varchar(255) NOT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=92 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `pass`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'admin', '$2y$08$2ARPIbr6jpuorpV8kXb5UuY577jjZ2wIsCBlZsXO6UUNvmGbc3fLe', NULL, '', NULL, NULL, NULL, NULL, 1447915390, 1474973917, 1, 'Super Admin', NULL, '', NULL, NULL),
(2, '127.0.0.1', 'manager', '$2y$08$mBPX0Xc/Hu/ITMis7Ki0RuUFZxDfrjDK9Cvky8zwYWkTOJNVQRWPa', NULL, 'admin@admin.com', NULL, NULL, NULL, NULL, 1447915390, 1475520914, 1, 'admin', NULL, '', NULL, NULL),
(3, '127.0.0.1', 'staff', '$2y$08$keb4OR6FzzI6ThYbb2tVtuVNQf9IEsc2FiZ5tJ1cwtX/UusBHrNkq', NULL, 'stylist@stylist.com', NULL, NULL, NULL, NULL, 1447915390, 1475038282, 1, 'Stylist', NULL, '', NULL, NULL),
(4, '127.0.0.1', 'member', '$2y$08$0EELOw5PYr0k43Rga2M4QOw.JF5WE68Qwp80KHHV1omwYUn.OYPOm', NULL, 'member@member.com', NULL, NULL, NULL, NULL, 1447915463, 1474968894, 1, 'Member', NULL, '', NULL, NULL),
(5, '', 'customer', '$2y$08$0EELOw5PYr0k43Rga2M4QOw.JF5WE68Qwp80KHHV1omwYUn.OYPOm', NULL, 'customer@customer.com', NULL, NULL, NULL, NULL, 1447915463, NULL, 1, NULL, NULL, '', NULL, NULL),
(33, '203.187.199.165', 'Victor', '$2y$08$lpPiHQLDQ7j3VK9.MeOCbe3PMpoG.HLPpoCBhp8S6BfBVlaPOjBrW', NULL, 'vir.j477@gmail.com', 'd0c08023c7ee100389744e6e94b74470d1d1c02e', NULL, NULL, NULL, 1475078447, 1475078752, 1, NULL, NULL, '', NULL, NULL),
(56, '', 'xxxx', '$2y$08$pQlujqq43dCRybcK8w.v.uJaDLYJMBm4FVPV/0B6si8oCmYt87f6S', NULL, 'abc@abc.com', NULL, NULL, NULL, NULL, 1475149237, 1475216724, 1, NULL, NULL, '9978498512', NULL, NULL),
(57, '', 'xyz', '$2y$08$ZQLmHU/WzojgHmIa3rGdeu5P5ZVajjz4sDojjGf0i9trcNAhlgzPO', NULL, 'xyz@xyz.com', NULL, NULL, NULL, NULL, 1475149266, 1475502067, 1, NULL, NULL, '9978498512', NULL, NULL),
(58, '', 'pqr', '$2y$08$s8DFpJ0k6p6pX3wCTYMo3OAocfgtFu3H4DdcyArmE2KhQ0p5mhpxy', NULL, 'pqr@pqr.com', NULL, NULL, NULL, NULL, 1475149301, 1475217847, 1, NULL, NULL, '123456789', NULL, NULL),
(65, '103.226.184.254', 'yyy', '$2y$08$1DbnPcH3YJomnuSn2Ct7Lu3.7hRz80UysTFauKAxOQBTXqEU9T4MW', NULL, 'yyy@yyy.com', '9fd13c1edeab2a9da09b03e4710a42071bd15e15', NULL, NULL, NULL, 1475155428, NULL, 1, NULL, NULL, '', NULL, NULL),
(66, '103.226.184.254', 'll', '$2y$08$xZ8hbNxsUAzAE1A2bky1SexB0ZL9dyRYzQbvyytiyl0ymjrbcarxq', NULL, 'lll@lll.com', '9be086a6f6b42816c61ea82881b18dd6931155f1', NULL, NULL, NULL, 1475155473, NULL, 1, NULL, NULL, '', NULL, NULL),
(68, '', 'jimmy', '$2y$08$PNMA0pZRPI1sPrJy2fhmjuAqQXsZOzbHHI9eDeoowT49/QkJ1hnfu', NULL, 'jimmy@xemesolutions.com', NULL, NULL, NULL, NULL, 1475251306, NULL, 1, NULL, NULL, '12345678', NULL, NULL),
(69, '14.195.232.205', 'test', '$2y$08$WcQdog7hbfPHbnR3HZzH/O3g6xxYhR2gGLKkxPbYXfPMb/OMxdKkK', NULL, 'test@gmail.com', 'a75eb7c65ad19050afa4b3c27cc3dec9016ae8c9', NULL, NULL, NULL, 1475497563, NULL, 0, NULL, NULL, '', NULL, NULL),
(91, '43.243.38.22', 'rajnik', '$2y$08$fyfM/5dQK8xvJHVVN00n7.X5hUTBncfQrINX6.V1GxjgsLVO6P006', NULL, 'rajnik@xemesolutions.com', NULL, NULL, NULL, NULL, 1475581875, 1475582302, 1, NULL, NULL, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE IF NOT EXISTS `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=86 ;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3),
(4, 4, 4),
(9, 5, 4),
(37, 33, 4),
(56, 56, 3),
(57, 57, 3),
(58, 58, 3),
(62, 65, 4),
(63, 66, 4),
(65, 68, 3),
(66, 69, 4),
(85, 91, 4);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
