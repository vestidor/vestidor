<?php 
class Products_model extends CI_Model  {
	
	function get_products()
	{
		$this->db->select('tb_products.*,tb_products_image.image');
		$this->db->from('tb_products');
		$this->db->join('tb_products_image', 'tb_products_image.productID = tb_products.ID');
		$query = $this->db->get();
		return $query->result();
	}
	function get_box($id)
	{
		$this->db->where('ID', $id);
		return $this->db->get('tb_box')->result();
	}
	function add_product($id,$pid)
	{
		$sql = "insert into tb_box_product (boxID,productID) value ('".$id."','".$pid."')";
		$query = $this->db->query($sql);
	}
	function delete_product($id)
	{
		$sql ="delete from tb_box_product where ID=$id";
		$query = $this->db->query($sql);
	}
	function select_product_id($id)
	{
		$sql = "select boxID from tb_box_product where ID=$id";
		$query = $this->db->query($sql);
		return $query->result();
	}
}
?>
