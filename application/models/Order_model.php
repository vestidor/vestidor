<?php 
class Order_model  extends CI_Model  {
	
	function get_box_id($users)
	{
		$sql = "select * from tb_box where userID=$users";
		$query = $this->db->query($sql);
		return $query->result();
	}
	function get_boxby_id($id)
	{
		$this->db->where('boxID', $id);
		return $this->db->get('tb_box_product')->result();
	}
	
	function insert_order($address,$boxID,$userID,$price)
	{
		$date = date('Y-m-d');
		$widthdrawl = date('Y-m-d', strtotime("+5 days"));
		
		$sql = "UPDATE tb_box SET orderDate='".$date."', withdrawDate='".$widthdrawl."', estimatedAmount='".$price."' where ID=$boxID";
		$query = $this->db->query($sql);
		
		$sqltwo = "insert into tb_shipment (userID,boxID,addressType,status) value ('".$userID."','".$boxID."','".$address."','1')";
		$querytwo = $this->db->query($sqltwo);
	}
	
	
	function insert_finalorder($boxid,$price,$users)
	{
		$date = date('Y-m-d h:i:s');
		$sql = "INSERT INTO tb_order (userID,amount,dateTimeofOrder) value ('".$users."','".$price."','".$date."')";
		$query = $this->db->query($sql);
		$insert_id = $this->db->insert_id();
		
		$box = "select productID from tb_box_product where boxID=$boxid";
		$boxquery = $this->db->query($box);
		$boxdata = $boxquery->result();
		
		foreach($boxdata as $b)
		{
			$productID = $b->productID;
			$sqlone = "insert into tb_orderproducts (orderID,boxID,productID) value ('".$insert_id."','".$boxid."','".$productID."')";
			$queryone = $this->db->query($sqlone);
		}
		
		$sqlthree = "UPDATE tb_box SET boxStatus = '1' where ID=$boxid";
		$querythree=$this->db->query($sqlthree);
		
	}

}
?>
