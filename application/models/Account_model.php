<?php 
class Account_model  extends CI_Model  {
	
	public function get_by_id($users)
	{
		$sql = "select * from users join tb_user_details on users.id=tb_user_details.userID where users.id=$users";
		$query = $this->db->query($sql);
		return $query->result();
	}
	function update($data,$id){
		$this->db->where('ID',$id);
		return $this->db->update('tb_user_details',$data);
	}
	function get_stylist_by_id($users)
	{
		$sql = "select * from users join tb_stylist_details on users.id=tb_stylist_details.userID where users.id=$users";
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	function update_stylist($data,$id,$stylist)
	{
		$this->db->where('userID',$id);
		$this->db->update('tb_stylist_details',$data);
		
		$this->db->where('id',$id);
		$this->db->update('users',$stylist);
	}
}
?>
