<?php 
class Shipment_model extends CI_Model
{
	function get_shipment_by_id($users)
	{
		$sql="select * from tb_shipment join tb_box on tb_box.ID=tb_shipment.boxID where tb_shipment.userID=$users";
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	function get_shipment_by_stylist($users)
	{
		$sql = "SELECT tb_shipment.*,tb_box.createdOn,tb_box.orderDate,tb_box.withdrawDate FROM tb_shipment JOIN tb_box ON tb_box.ID = tb_shipment.boxID JOIN tb_user_details ON tb_user_details.userID = tb_shipment.userID join tb_stylist_details on tb_stylist_details.ID=tb_user_details.stylistID where tb_stylist_details.userID=$users";
		$query = $this->db->query($sql);
		return $query->result();
	}
}
?>