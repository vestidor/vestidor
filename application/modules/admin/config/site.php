<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Site (by CI Bootstrap 3)
| -------------------------------------------------------------------------
| This file lets you define default values to be passed into views when calling 
| MY_Controller's render() function. 
|
| Each of them can be overrided from child controllers.
|
*/

$config['site'] = array(

	// Site name
	'name' => 'Vestidor Admin',

	// Default page title
	// (set empty then MY_Controller will automatically generate one according to controller / action)
	'title' => '',

	// Multilingual settings (set empty array to disable this)
	'multilingual' => array(),

	// User groups that is allowed to login Admin Panel;
	// each group will have different AdminLTE skin
	'authorized_groups' => array(
		'manager'	=> array('adminlte_skin' => 'skin-purple')
	),

	// Menu items which support icon fonts, e.g. Font Awesome
	// (or directly update view file: /application/modules/admin/views/_partials/sidemenu.php)
	'menu' => array(
		'home' => array(
			'groups'	=> array('admin', 'manager', 'staff'),
			'name'		=> 'Home',
			'url'		=> '',
			'icon'		=> 'fa fa-home',
		),
		
		'stylist' => array(
			'groups'	=> array('admin'),
			'name'		=> 'Stylist',
			'url'		=> '',
			'icon'		=> 'fa fa-users',
			'children'  => array(
				'Stylist Listing'	=> 'stylist',
				'Add Stylist'		=> 'stylist/stylist_add',
			),
		),
		
		'customers' => array(
			'groups'	=> array('manager'),
			'name'		=> 'Customers',
			'url'		=> 'customers',
			'icon'		=> 'fa fa-user',
		),
		
		'sales' => array(
			'groups'	=> array('admin'),
			'name'		=> 'Sales',
			'url'		=> '',
			'icon'		=> 'fa fa-file-text-o',
			'children'  => array(
				'Order'		=> 'order',
				'Invoice'	=> 'invoice',
				'Shipment'  => 'shipment',
			),
		),
		
		'products' => array(
			'groups'	=> array('admin'),
			'name'		=> 'Products',
			'url'		=> '',
			'icon'		=> 'fa fa-cart-arrow-down',
			'children'  => array(
				'Product Listing'	=> 'products',
				'Add Product'		=> 'products/productsadd',
			),
		),
		
		'brand' => array(
			'groups'	=> array('admin'),
			'name'		=> 'Brand',
			'url'		=> '',
			'icon'		=> 'fa fa-paper-plane',
			'children'  => array(
				'Brand Listing'		=> 'brand',
				'Add Brand'			=> 'brand/brandadd',
			),
		),
		
		'category' => array(
			'groups'	=> array('admin'),
			'name'		=> 'Category',
			'url'		=> '',
			'icon'		=> 'fa fa-file-code-o',
			'children'  => array(
				'Category Listing'		=> 'category',
				'Add Category'			=> 'category/categoryadd',
			),
		),
		
		'productimage' => array( 
			'groups'	=> array('admin'),
			'name'		=> 'Product Image',
			'url'		=> '',
			'icon'		=> 'fa fa-picture-o',
			'children'  => array(
				'Product Img Listing'		=> 'productimage',
				'Add Product Img'			=> 'productimage/index/add',
			),
		),
		
		'reports' => array(
			'groups'	=> array('manager'),
			'name'		=> 'Reports',
			'url'		=> '',
			'icon'		=> 'fa fa-file-pdf-o',
		),
					
		/*'cover_photo' => array(
			'groups'	=> array('admin'),
			'name'		=> 'Cover Photos',
			'url'		=> 'cover_photo',
			'icon'		=> 'fa fa-photo',
		),*/
		/*'blog' => array(
			'groups'	=> array('admin'),
			'name'		=> 'Blog',
			'url'		=> 'blog',
			'icon'		=> 'fa fa-pencil',
			'children'  => array(
				'Posts'			=> 'blog/post',
				'Categories'	=> 'blog/category',
				'Tags'			=> 'blog/tag',
			),
		),*/
		/*'accreditation' => array(
			'groups'	 => array('admin'),
			'name'		 => 'Accreditation',
			'url'		 => 'accreditation',
			'icon'		 => 'fa fa-pencil',
			'children'  => array(
				'All Questions'	=> 'accreditation',
				'Add Question'	=> 'accreditation/add_question',
				'Requests'		=> 'accreditation/all_requests',
				),
			),
			
		'jobs' => array(
			'groups'	 => array('admin'),
			'name'		 => 'Jobs Management',
			'url'		 => '',
			'icon'		 => 'fa fa-check-square-o',
			'children'  => array(
				'Job Categories'	=> 'employer/job_category',
				'Job Skills'	=> 'employer/job_skills',
				'All Jobs'	=>	'employer/all_jobs'
				),
			),*/
		
		/*'demo' => array(
			'groups'	=> array('admin', 'manager', 'staff'),
			'name'		=> 'Demo',
			'url'		=> 'demo',
			'icon'		=> 'ion ion-gear-b',	// use Ionicons (instead of FontAwesome)
			'children'  => array(
				'Pagination'	=> 'demo/pagination',
				'Sortable'		=> 'demo/sortable',
				'Item 1'		=> 'demo/item/1',
				'Item 2'		=> 'demo/item/2',
				'Item 3'		=> 'demo/item/3',
			)
		),*/
		'logout' => array(
			'groups'	=> array('admin', 'manager', 'staff'),
			'name'		=> 'Sign Out',
			'url'		=> 'account/logout',
			'icon'		=> 'fa fa-sign-out',
		)
	),

	// Useful links to display at bottom of sidemenu (e.g. to pages outside Admin Panel)
	'useful_links' => array(
		/*array(
			'groups'	=> array('admin', 'manager', 'staff'),
			'name'		=> 'Frontend Website',
			'url'		=> '',
			'target'	=> '_blank',
			'color'		=> 'text-aqua'
		),
		array(
			'groups'	=> array('admin'),
			'name'		=> 'API Site',
			'url'		=> 'api',
			'target'	=> '_blank',
			'color'		=> 'text-orange'
		),
		array(
			'groups'	=> array('admin', 'manager', 'staff'),
			'name'		=> 'Github Repo',
			'url'		=> CI_BOOTSTRAP_REPO,
			'target'	=> '_blank',
			'color'		=> 'text-green'
		),*/
	),

	// For debug purpose (available only when ENVIRONMENT = 'development')
	'debug' => array(
		'view_data'		=> FALSE,	// whether to display MY_Controller's mViewData at page end
		'profiler'		=> FALSE,	// whether to display CodeIgniter's profiler at page end
	),
);
