<?php 
class Brand_model  extends CI_Model  {
	
	function save($data)
	{
		$msg = $this->db->insert('tb_brand', $data);
	}
	function get_all_id($users){
		$this->db->where('adminID', $users);
		return $this->db->get('tb_brand')->result();
	}
	function delete($id){
		$this->db->where('ID',$id);
		$this->db->delete('tb_brand');
	}
	
	function get($id){
		$this->db->where('ID', $id);
		return $this->db->get('tb_brand')->result();
	}
	function update($data,$id){
		$this->db->where('ID',$id);
		return $this->db->update('tb_brand',$data);
	}
}
?>