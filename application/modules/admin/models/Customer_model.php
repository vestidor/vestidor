<?php 
class Customer_model  extends CI_Model  {
	
	function get_all(){
		$this->db->select('users.*');
		$this->db->from('users');
		$this->db->join('users_groups', 'users_groups.user_id = users.id');
		$this->db->where('users_groups.group_id',4); 
		$query = $this->db->get();
		return $query->result();
	}
	function active($data,$id){
		$this->db->where('id',$id);
		return $this->db->update('users',$data);
	}
	function deactive($data,$id){
		$this->db->where('id',$id);
		return $this->db->update('users',$data);
	}
	
}
?>