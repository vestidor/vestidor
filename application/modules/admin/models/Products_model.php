<?php 
class Products_model  extends CI_Model  {
	
	function save($data)
	{
		$msg = $this->db->insert('tb_products', $data);
	}
	function get_all_id($users){
		$this->db->where('adminID', $users);
		return $this->db->get('tb_products')->result();
	}
	function delete($id){
		$this->db->where('ID',$id);
		$this->db->delete('tb_products');
	}
	
	function get($id){
		$this->db->where('ID', $id);
		return $this->db->get('tb_products')->result();
	}
	function update($data,$id){
		$this->db->where('ID',$id);
		return $this->db->update('tb_products',$data);
	}
}
?>