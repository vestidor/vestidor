<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_builder');
		$this->load->library('session');
		$this->load->helper('url');
	}
	
	
	public function index()
	{
		$ci = &get_instance();
		$ci -> load -> library('session');
		$x = $ci -> session -> userdata('user_group');
		$y = $ci -> session -> userdata();
		$users = $y['user_id'];
		$this->load->model('products_model');
		$data=$this->products_model->get_all_id($users);
		$this->load->model('brand_model');
		$datatwo=$this->brand_model->get_all_id($users);
		$this->load->model('category_model');
		$datathree=$this->category_model->get_all_id($users);
		$this->mViewData['category']=$datathree;
		$this->mViewData['brand']=$datatwo; 
		$this->mViewData['result']=$data;
		$this->render('products/index');
	}
	public function productsadd()
	{
		$ci = &get_instance();
		$ci -> load -> library('session');
		$x = $ci -> session -> userdata('user_group');
		$y = $ci -> session -> userdata();
		$users = $y['user_id'];
		$this->load->model('brand_model');
		$datatwo=$this->brand_model->get_all_id($users);
		$this->load->model('category_model');
		$datathree=$this->category_model->get_all_id($users);
		$this->mViewData['category']=$datathree;
		$this->mViewData['brand']=$datatwo; 
		$this->render('products/add');
	}
	public function insert()
	{
		$dat = $_POST;
		$data['name'] = $this->input->post('name');
		$data['sku'] = $this->input->post('sku');
		$data['status'] = $this->input->post('status');
		$data['brandName'] = $this->input->post('brand');
		$data['category'] = $this->input->post('category');
		$data['simplePrice'] = $this->input->post('simplePrice');
		$data['discountPrice'] = $this->input->post('discountPrice');
		$data['shortDes'] = $this->input->post('short');
		$data['description'] = $this->input->post('long');
		$data['quantity'] = $this->input->post('quantity');
		$data['adminID'] = $this->input->post('aid');
		$this->load->model('products_model');
		$id=$this->products_model->save($data);
		$this->session->set_flashdata('submit','success');
		redirect('admin/products');
	}
	public function delete($id)
	{
		$this->load->model('products_model');
		$this->products_model->delete($id);
		redirect('admin/products');
	}
	
	public function edit($id)
	{
		$ci = &get_instance();
		$ci -> load -> library('session');
		$x = $ci -> session -> userdata('user_group');
		$y = $ci -> session -> userdata();
		$users = $y['user_id'];
		$this->load->model('brand_model');
		$datatwo=$this->brand_model->get_all_id($users);
		$this->load->model('category_model');
		$datathree=$this->category_model->get_all_id($users);
		$this->load->model('products_model');
		$data = $this->products_model->get($id);
		$this->mViewData['category']=$datathree;
		$this->mViewData['brand']=$datatwo; 
		$this->mViewData['result']=$data;
		$this->render('products/edit');
	}
	public function update()
	{
		$id = $this->input->post('id');
		$data['ID'] = $this->input->post('id');
		$data['name'] = $this->input->post('name');
		$data['sku'] = $this->input->post('sku');
		$data['status'] = $this->input->post('status');
		$data['brandName'] = $this->input->post('brand');
		$data['category'] = $this->input->post('category');
		$data['simplePrice'] = $this->input->post('simplePrice');
		$data['discountPrice'] = $this->input->post('discountPrice');
		$data['shortDes'] = $this->input->post('short');
		$data['description'] = $this->input->post('long');
		$data['quantity'] = $this->input->post('quantity');
		$data['adminID'] = $this->input->post('aid');
		$this->load->model('products_model');
		$id=$this->products_model->update($data,$id);
		$this->session->set_flashdata('submit','success');
		redirect('admin/products');
	}
}
?>