<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_builder');
		$this->load->library('session');
		$this->load->helper('url');
	}
	
	
	public function index()
	{
		$ci = &get_instance();
		$ci -> load -> library('session');
		$x = $ci -> session -> userdata('user_group');
		$y = $ci -> session -> userdata();
		$users = $y['user_id'];
		$this->load->model('category_model');
		$data=$this->category_model->get_all_id($users);
		$this->mViewData['result']=$data;
		$this->render('category/index');
	}
	public function categoryadd()
	{
		$this->render('category/add');
	}
	public function insert()
	{
		$dat = $_POST;
		$data['categoryName'] = $this->input->post('category');
		$data['adminID'] = $this->input->post('aid');
		$this->load->model('category_model');
		$id=$this->category_model->save($data);
		$this->session->set_flashdata('submit','success');
		redirect('admin/category');
	}
	public function delete($id)
	{
		$this->load->model('category_model');
		$this->category_model->delete($id);
		redirect('admin/category');
	}
	
	public function edit($id)
	{
		$this->load->model('category_model');
		$data = $this->category_model->get($id);
		$this->mViewData['result']=$data;
		$this->render('category/edit');
	}
	public function update()
	{
		$id = $this->input->post('id');
		$data['ID'] = $this->input->post('id');
		$data['categoryName'] = $this->input->post('category');
		$data['adminID'] = $this->input->post('aid');
		$this->load->model('category_model');
		$id=$this->category_model->update($data,$id);
		$this->session->set_flashdata('submit','success');
		redirect('admin/category');
	}
}