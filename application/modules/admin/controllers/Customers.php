<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customers extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_builder');
		$this->load->library('session');
		$this->load->helper('url');
	}
	
	public function index()
	{
		$this->load->model('customer_model');
		$data=$this->customer_model->get_all();
		$this->mViewData['result']=$data;
		$this->render('customers/customers_listing');
	}
	public function customers_deactive($id)
	{
		$data['active'] = 1;
		$this->load->model('customer_model');
		$id=$this->customer_model->deactive($data,$id);
		redirect('admin/customers');
	}
	public function customers_active($id)
	{
		$data['active'] = 0;
		$this->load->model('customer_model');
		$id=$this->customer_model->active($data,$id);
		redirect('admin/customers');
	}
}