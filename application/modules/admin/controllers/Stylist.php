<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stylist extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_builder');
		$this->load->library('session');
		$this->load->helper('url');
	}

	public function index()
	{
		$ci = &get_instance();
		$ci -> load -> library('session');
		$x = $ci -> session -> userdata('user_group');
		$y = $ci -> session -> userdata();
		$users = $y['user_id'];
		$this->load->model('stylist_model');
		$data=$this->stylist_model->get_all_id($users);
		$this->mViewData['result']=$data;
		$this->render('stylist/stylist_listing');
	}

	public function stylist_deactive($id)
	{
		$data['active'] = 1;
		$this->load->model('stylist_model');
		$id=$this->stylist_model->deactive($data,$id);
		redirect('admin/stylist');
	}

	public function stylist_active($id)
	{
		$data['active'] = 0;
		$this->load->model('stylist_model');
		$id=$this->stylist_model->active($data,$id);
		redirect('admin/stylist');
	}

	public function stylist_add()
	{
		$this->render('stylist/stylist_add');
	}
	
	public function insert()
	{
		$data = $_POST;
		
		$adminid = $this->input->post('aid');
		$username = $this->input->post('username');
		$email = $this->input->post('email');
		$pass = $this->input->post('pass');
		$phoneOne = $this->input->post('phoneOne');
		$phoneTwo = $this->input->post('phoneTwo');
		$address = $this->input->post('address');
		$this->load->model('stylist_model');
		$data=$this->stylist_model->save($adminid,$username,$email,$pass,$phoneOne,$phoneTwo,$address);
		redirect('admin/stylist');
	}
	
	
	public function stylist_edit($id)
	{
		$this->load->model('stylist_model');
		$data=$this->stylist_model->get_id($id);
		$this->mViewData['result']=$data;
		$this->render('stylist/stylist_edit');
	}
	
	public function update()
	{
		$id = $this->input->post('id');
		$adminid = $this->input->post('aid');
		$username = $this->input->post('username');
		$email = $this->input->post('email');
		$pass = $this->input->post('pass');
		$phoneOne = $this->input->post('phoneOne');
		$phoneTwo = $this->input->post('phoneTwo');
		$address = $this->input->post('address');
		$this->load->model('stylist_model');
		$data=$this->stylist_model->update($id,$adminid,$username,$email,$pass,$phoneOne,$phoneTwo,$address);
		redirect('admin/stylist');
	}
	
	public function stylist_delete($id)
	{
		$this->load->model('stylist_model');
		$this->stylist_model->delete($id);
		redirect('admin/stylist');
	}
}
?>