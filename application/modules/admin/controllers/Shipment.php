<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shipment extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_builder');
		$this->load->library('session');
		$this->load->helper('url');
	}
	
	
	public function index()
	{
		$this->load->model('shipment_model');
		$data=$this->shipment_model->get_ship();
		$this->mViewData['result']=$data;
		$this->render('shipment/shipment');
	}
	public function delivery($id)
	{
		$this->load->model('shipment_model');
		$data=$this->shipment_model->delivery_status($id);
		redirect('admin/shipment');
	}
	
	public function delivered($id)
	{
		$this->load->model('shipment_model');
		$data=$this->shipment_model->delivered_status($id);
		redirect('admin/shipment');
	}
}