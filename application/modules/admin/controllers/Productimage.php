<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productimage extends Admin_Controller {

	public function index()
	{
		$crud = $this->crud->generate_crud('tb_products_image');
		$crud->required_fields('image','productID');
		$crud->display_as('productID','Product Name');
		$crud->set_relation('productID','tb_products','name');
		$crud->set_field_upload('image','./uploads/');
    	$this->mViewData['crud_data'] = $this->crud->render();
		$this->render('crud');
	}
}
?>