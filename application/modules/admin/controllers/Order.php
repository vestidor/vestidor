<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends Admin_Controller {
	
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_builder');
		$this->load->library('session');
		$this->load->helper('url');
	}
	
	
	public function index()
	{
		$this->render('order/order');
	}
	
}
?>
