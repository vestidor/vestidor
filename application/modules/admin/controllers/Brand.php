<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Brand extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_builder');
		$this->load->library('session');
		$this->load->helper('url');
	}
	
	
	public function index()
	{
		$ci = &get_instance();
		$ci -> load -> library('session');
		$x = $ci -> session -> userdata('user_group');
		$y = $ci -> session -> userdata();
		$users = $y['user_id'];
		$this->load->model('brand_model');
		$data=$this->brand_model->get_all_id($users);
		$this->mViewData['result']=$data;
		$this->render('brand/index');
	}
	public function brandadd()
	{
		$this->render('brand/add');
	}
	public function insert()
	{
		$dat = $_POST;
		$data['brandName'] = $this->input->post('brand');
		$data['adminID'] = $this->input->post('aid');
		$this->load->model('brand_model');
		$id=$this->brand_model->save($data);
		$this->session->set_flashdata('submit','success');
		redirect('admin/brand');
	}
	public function delete($id)
	{
		$this->load->model('brand_model');
		$this->brand_model->delete($id);
		redirect('admin/brand');
	}
	
	public function edit($id)
	{
		$this->load->model('brand_model');
		$data = $this->brand_model->get($id);
		$this->mViewData['result']=$data;
		$this->render('brand/edit');
	}
	public function update()
	{
		$id = $this->input->post('id');
		$data['ID'] = $this->input->post('id');
		$data['brandName'] = $this->input->post('brand');
		$data['adminID'] = $this->input->post('aid');
		$this->load->model('brand_model');
		$id=$this->brand_model->update($data,$id);
		$this->session->set_flashdata('submit','success');
		redirect('admin/brand');
	}
}