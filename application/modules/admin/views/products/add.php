<?php $this->layout('layouts::default') ?>
<?php 
	$ci =& get_instance();
	$ci->load->library('session');
/*	print_r($ci->session->userdata());
	echo"<br>"; */
	$x=$ci->session->userdata('user_group');
	$y=$ci->session->userdata();
	$users= $y['user_id'];
?>
<div class="row">
	<div class="col-xs-12">
    	<div class="box">
            <div class="box-header">
              	<h3 class="box-title">Brand Add</h3>
              	<hr />
              	<form action="products/insert" method="post" accept-charset="utf-8" enctype="multipart/form-data">
					<input type="hidden" name="aid" value="<?php echo $users; ?>" />	
				<div class="form-group col-md-6">
					<label for="email">Name</label>
					<input type="text" name="name" class="form-control input-xs" />
				</div>
				
				<div class="form-group col-md-6">
					<label for="email">SKU</label>
					<input type="text" name="sku" class="form-control input-xs" />
				</div>
				<div class="form-group col-md-6">
					<label for="email">Status</label>
					<select name="status" class="form-control input-xs">
						<option value="">Select</option>
						<option value="0">No</option>
						<option value="1">Yes</option>
					</select>	
				</div>
				
				<div class="form-group col-md-6">
					<label for="email">Brand</label>
					<select name="brand" class="form-control input-xs">
						<option value="">Select</option>
						<?php foreach($brand as $b) { ?>
						<option value="<?php echo $b->ID; ?>"><?php echo $b->brandName; ?></option>
						<?php } ?>
					</select>	
				</div>
				
				<div class="form-group col-md-6">
					<label for="email">Category</label>
					<select name="category" class="form-control input-xs">
						<option value="">Select</option>
						<?php foreach($category as $c) { ?>
						<option value="<?php echo $c->ID; ?>"><?php echo $c->categoryName; ?></option>
						<?php } ?>
					</select>	
				</div>
				
				<div class="form-group col-md-6">
					<label for="email">Quantity</label>
					<input type="text" name="quantity" class="form-control input-xs" />
				</div>
				
				<div class="form-group col-md-6">
					<label for="email">Simple Price</label>
					<input type="text" name="simplePrice" class="form-control input-xs" />
				</div>
				
				<div class="form-group col-md-6">
					<label for="email">Discount Price</label>
					<input type="text" name="discountPrice" class="form-control input-xs" />
				</div>
				
				<div class="form-group col-md-6">
					<label for="email">Short Description</label>
					<textarea name="short" class="form-control"></textarea>	
				</div>
				
				<div class="form-group col-md-6">
					<label for="email">Long Description</label>
					<textarea name="long" class="form-control"></textarea>	
				</div>
				
				<div class="form-group col-md-12">
					<input type="submit" name="add" value="Add" class="btn btn-primary" />
				</div>	
              		
              	</form>	
              	
			</div>
		</div>
	</div>
</div>					