<?php $this->layout('layouts::default') ?>
<?php 
	$ci =& get_instance();
	$ci->load->library('session');
/*	print_r($ci->session->userdata());
	echo"<br>"; */
	$x=$ci->session->userdata('user_group');
	$y=$ci->session->userdata();
	$users= $y['user_id'];
?>
<div class="row">
	<div class="col-xs-12">
    	<div class="box">
            <div class="box-header">
              	<h3 class="box-title">Stylist Add</h3>
              	<hr />
              	<form action="stylist/insert" method="post" accept-charset="utf-8" enctype="multipart/form-data">
					<input type="hidden" name="aid" value="<?php echo $users; ?>" />	
				<div class="form-group col-md-6">
					<label for="email">Name</label>
					<input type="text" name="username" class="form-control input-xs" />
				</div>
				
				<div class="form-group col-md-6">
					<label for="email">Email</label>
					<input type="text" name="email" class="form-control input-xs" />
				</div>
				
				<div class="form-group col-md-6">
					<label for="email">Password</label>
					<input type="password" name="pass" class="form-control input-xs" />
				</div>
				
				<div class="form-group col-md-6">
					<label for="email">Phone Number One</label>
					<input type="text" name="phoneOne" class="form-control input-xs" />
				</div>
				
				<div class="form-group col-md-6">
					<label for="email">Address</label>
					<input type="text" name="address" class="form-control input-xs" />
				</div>
			
				<div class="form-group col-md-6">
					<label for="email">Phone Number Two</label>
					<input type="text" name="phoneTwo" class="form-control input-xs" />
				</div>
			
				<div class="form-group col-md-6">
					<input type="submit" name="add" value="Add" class="btn btn-primary" />
				</div>	
              		
              	</form>	
              	
			</div>
		</div>
	</div>
</div>					