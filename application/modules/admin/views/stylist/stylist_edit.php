<?php $this->layout('layouts::default') ?>
<?php 
	$ci =& get_instance();
	$ci->load->library('session');
/*	print_r($ci->session->userdata());
	echo"<br>"; */
	$x=$ci->session->userdata('user_group');
	$y=$ci->session->userdata();
	$users= $y['user_id'];
?>
<div class="row">
	<div class="col-xs-12">
    	<div class="box">
            <div class="box-header">
              	<h3 class="box-title">Stylist Edit</h3>
              	<hr />
              	<form action="stylist/update" method="post" accept-charset="utf-8" enctype="multipart/form-data">
				<?php foreach($result as $res) { ?>
				<input type="hidden" name="id" value="<?php echo $res->id; ?>" />
				<input type="hidden" name="aid" value="<?php echo $users; ?>" />	
				<div class="form-group col-md-6">
					<label for="email">Name</label>
					<input type="text" name="username" value="<?php echo $res->username; ?>" class="form-control input-xs" />
				</div>
				
				<div class="form-group col-md-6">
					<label for="email">Email</label>
					<input type="text" name="email" value="<?php echo $res->email; ?>" class="form-control input-xs" />
				</div>
				
				<div class="form-group col-md-6">
					<label for="email">Password</label>
					<input type="text" name="pass" value="<?php echo $res->pass; ?>" class="form-control input-xs" />
				</div>
				
				<div class="form-group col-md-6">
					<label for="email">Phone Number One</label>
					<input type="text" name="phoneOne" value="<?php echo $res->phoneOne; ?>" class="form-control input-xs" />
				</div>
				
				<div class="form-group col-md-6">
					<label for="email">Address</label>
					<input type="text" name="address" value="<?php echo $res->address; ?>" class="form-control input-xs" />
				</div>
			
				<div class="form-group col-md-6">
					<label for="email">Phone Number Two</label>
					<input type="text" name="phoneTwo" value="<?php echo $res->phoneTwo; ?>" class="form-control input-xs" />
				</div>
				<?php } ?>
				<div class="form-group col-md-6">
					<input type="submit" name="add" value="Edit" class="btn btn-primary" />
				</div>	
              		
              	</form>	
              	
			</div>
		</div>
	</div>
</div>					