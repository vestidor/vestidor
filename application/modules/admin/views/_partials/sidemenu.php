<!--<?php 
	$ci =& get_instance();
	$ci->load->library('session');
/*	print_r($ci->session->userdata());
	echo"<br>"; */
	$x=$ci->session->userdata('user_group');
	$y=$ci->session->userdata();
	$users= $y['user_id'];
?>-->
<ul class="sidebar-menu">

	<!--  <li class="header">MAIN NAVIGATION</li>-->

	<?php foreach ($menu as $parent => $parent_params): ?>

		<?php if (empty($parent_params['children'])): ?>

			<?php $active = ($current_uri==$parent_params['url'] || $ctrler==$parent); ?>
			<li class='<?php if ($active) echo 'active'; ?>'>
				<a href='<?php echo $parent_params['url']; ?>'>
					<i class='<?php echo $parent_params['icon']; ?>'></i>
					<span> 
					<?php echo $parent_params['name']; ?>
					</span>
				</a>
			</li>

		<?php else: ?>

			<?php $parent_active = ($ctrler==$parent); ?>
			<li class='treeview <?php if ($parent_active) echo 'active'; ?>'>
				<a href='#'>
					<i class='<?php echo $parent_params['icon']; ?>'></i> 
					<span><?php echo $parent_params['name']; ?></span>
					 
					<i class='fa fa-angle-left pull-right'></i>
				</a>
				<ul class='treeview-menu'>
					<?php foreach ($parent_params['children'] as $name => $url): ?>
						<?php $child_active = ($current_uri==$url); ?>
						<li <?php if ($child_active) echo 'class="active"'; ?>>
							<a href='<?php echo $url; ?>'><i class='fa fa-circle-o'></i> <?php echo $name; ?></a>
						</li>
					<?php endforeach; ?>
				</ul>
			</li>

		<?php endif; ?>

	<?php endforeach; ?>
	
	<?php if ( !empty($useful_links) ): ?>
		<li class="header">USEFUL LINKS</li>
		<?php foreach ($useful_links as $link): ?>
			<li>
				<a href="<?php echo starts_with($link['url'], 'http') ? $link['url'] : base_url($link['url']); ?>" target='<?php echo $link['target']; ?>'>
					<i class="fa fa-circle-o <?php echo $link['color']; ?>"></i> <?php echo $link['name']; ?>
				</a>
			</li>
		<?php endforeach; ?>
	<?php endif; ?>
	
<?php if($x[0]->group_id == 1){ ?>
	<li class="">
		<a href="account/logout">
			<i class="fa fa-sign-out"></i>
			<span> Sign Out </span>
		</a>
	</li>
<?php } ?>
<!--
<?php if($x[0]->group_id == 2){ ?>
	<li class='treeview '>
				<a href='#'>
					<i class='fa fa-users'></i> 
					<span>Stylist</span>
					<i class='fa fa-angle-left pull-right'></i>
				</a>
				<ul class='treeview-menu'>
						<li >
							<a href='#'><i class='fa fa-circle-o'></i>Stylist Listing</a>
						</li>
						<li >
							<a href='#'><i class='fa fa-circle-o'></i>Add Stylist</a>
						</li>
				</ul>
	</li>
	<li class='treeview '>
				<a href='#'>
					<i class='fa fa-file-text-o'></i> 
					<span>sales</span>
					<i class='fa fa-angle-left pull-right'></i>
				</a>
				<ul class='treeview-menu'>
						<li >
							<a href='#'><i class='fa fa-circle-o'></i>Order</a>
						</li>
						<li >
							<a href='#'><i class='fa fa-circle-o'></i>Invoice</a>
						</li>
						<li >
							<a href='#'><i class='fa fa-circle-o'></i>Shipment</a>
						</li>
				</ul>
	</li>
	<li class="treeview">
		<a href="#">
			<i class="fa fa-paper-plane"></i>
			<span> Brand </span>
			<i class='fa fa-angle-left pull-right'></i>
		</a>
		<ul class='treeview-menu'>
						<li >
							<a href='brand'><i class='fa fa-circle-o'></i>Brand Listing</a>
						</li>
						<li >
							<a href='brand/brandadd'><i class='fa fa-circle-o'></i>Add Brand</a>
						</li>
		</ul>
	</li>
	<li class="treeview">
		<a href="category">
			<i class="fa fa-file-code-o"></i>
			<span> Category </span>
			<i class='fa fa-angle-left pull-right'></i>
		</a>
		<ul class='treeview-menu'>
			<li >
				<a href='category'><i class='fa fa-circle-o'></i>Category Listing</a>
			</li>
			<li >
				<a href='category/categoryadd'><i class='fa fa-circle-o'></i>Add Category</a>
			</li>
		</ul>
	</li>
	<li class="treeview">
		<a href="products">
			<i class="fa fa-cart-arrow-down"></i>
			<span> Products </span>
			<i class='fa fa-angle-left pull-right'></i>
		</a>
		<ul class='treeview-menu'>
			<li >
				<a href='product'><i class='fa fa-circle-o'></i>Product Listing</a>
			</li>
			<li >
				<a href='product/productadd'><i class='fa fa-circle-o'></i>Add Product</a>
			</li>
		</ul>
	</li>
	<li class="treeview">
		<a href="productimage">
			<i class="fa fa-picture-o"></i>
			<span> Product Image </span>
			<i class='fa fa-angle-left pull-right'></i>
		</a>
		<ul class='treeview-menu'>
			<li >
				<a href='#'><i class='fa fa-circle-o'></i>Product Img Listing</a>
			</li>
			<li >
				<a href='#'><i class='fa fa-circle-o'></i>Add Product Img</a>
			</li>
		</ul>
	</li>
	<li class="">
		<a href="productimage">
			<i class="fa fa-user"></i>
			<span> Customers </span>
		</a>
	</li>
	<li class="">
		<a href="productimage">
			<i class="fa fa-file-pdf-o"></i>
			<span> Reports </span>
		</a>
	</li>
	
	<li class="">
		<a href="account/logout">
			<i class="fa fa-sign-out"></i>
			<span> Sign Out </span>
		</a>
	</li>
	
<?php } ?>

<?php if($x[0]->group_id == 3){ ?>
	<li class="">
		<a href="account/logout">
			<i class="fa fa-sign-out"></i>
			<span> Sign Out </span>
		</a>
	</li>	
<?php } ?>-->
</ul>