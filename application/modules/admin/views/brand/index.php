<?php $this->layout('layouts::default') ?>
<?php 
	$ci =& get_instance();
	$ci->load->library('session');
/*	print_r($ci->session->userdata());
	echo"<br>"; */
	$x=$ci->session->userdata('user_group');
	$y=$ci->session->userdata();
	$users= $y['user_id'];
?>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.0/js/responsive.bootstrap.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
<style>
.dataTables_paginate.paging_simple_numbers
{
	text-align: right;
}
.pagination
{
	margin: 0px;
}
.dataTables_filter
{
	text-align: right;
}
#example_wrapper
{
	margin-top: 20px;
}
</style>
<div class="row">
	<div class="col-xs-12">
    	<div class="box">
            <div class="box-header">
              	<h3 class="box-title">Brand Listing</h3>
               	<div class="box-tools">
                	<div class="input-group input-group-sm pull-right">
                		<a href="brand/brandadd" style="background-color: #605CA8; color: #FFFFFF" class="btn btn-sm">
                			<i class="fa fa-plus" aria-hidden="true"> </i>
							<span>Brand</span> 
                		</a>
					</div>
					
				</div>
			
				<table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">	
  						<thead>
    						<tr>
    							<th>Brand Name</th>	
      							<th style="text-align: center;">Action</th>
    						</tr>
  						</thead>
  						<tfoot>
  							<tr>
    							<th>Brand Name</th>	
      							<th style="text-align: center;">Action</th>
    						</tr>	
  						</tfoot>  	
  						<tbody>
  							<?php foreach ($result as $res) { ?>
    						<tr>	
    							<td><?php echo $res->brandName; ?></td>
      							<td style="text-align: center;">
      								<a class="btn btn-sm " style="padding: 0px 0px !important;" href="brand/edit/<?php echo $res->ID; ?>">
										<small class="label pull-right bg-green" style="padding: 8px;">
											<i class="fa fa-pencil" aria-hidden="true"></i>
										</small>
									</a>
     		 						<a class="btn btn-sm" style="padding: 0px 0px !important;" href="brand/delete/<?php echo $res->ID; ?>">
										<small class="label pull-right bg-red" style="padding: 8px;">
											<i class="fa fa-trash" aria-hidden="true"></i>
										</small>
									</a>
								</td>
     		 				</tr>
     		 				<?php } ?>	
     		 			</tbody>
				</table>		
				
			</div>
		</div>
	</div>
</div>					