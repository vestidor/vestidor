<?php $this->layout('layouts::default') ?>
<?php 
	$ci =& get_instance();
	$ci->load->library('session');
/*	print_r($ci->session->userdata());
	echo"<br>"; */
	$x=$ci->session->userdata('user_group');
	$y=$ci->session->userdata();
	$users= $y['user_id'];
?>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.0/js/responsive.bootstrap.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
<style>
.dataTables_paginate.paging_simple_numbers
{
	text-align: right;
}
.pagination
{
	margin: 0px;
}
.dataTables_filter
{
	text-align: right;
}
#example_wrapper
{
	margin-top: 20px;
}
</style>
<div class="row">
	<div class="col-xs-12">
    	<div class="box">
            <div class="box-header">
              	<h3 class="box-title">Customers Listing</h3>
              	<hr />
               	<!--div class="box-tools">
                	<div class="input-group input-group-sm pull-right">
                		<a href="stylist/stylist_add" style="background-color: #605CA8; color: #FFFFFF" class="btn btn-sm">
                			<i class="fa fa-plus" aria-hidden="true"> </i>
							<span>Stylist</span> 
                		</a>
				</div>	
				</div-->
			
				<table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">	
  						<thead>
    						<tr>
    							<th>Name</th>	
    							<th>Email</th>
      							<th style="text-align: center;">Status</th>
    						</tr>
  						</thead>
  						<tfoot>
  							<tr>
    							<th>Name</th>	
      							<th>Email</th>
      							<th style="text-align: center;">Status</th>
    						</tr>	
  						</tfoot>  	
  						<tbody>
  							<?php foreach($result as $res){ ?>
    						<tr>	
    							<td><?php echo $res->username; ?></td>
    							<td><?php echo $res->email; ?></td>
      							<td style="text-align: center;">
      								<?php if($res->active == 1) { ?>
      								<a class="btn btn-sm " style="padding: 0px 0px !important;" href="customers/customers_active/<?php echo $res->id; ?>">
										<small class="label pull-right bg-green" style="padding: 8px;">
											<i class="fa fa-thumbs-up" aria-hidden="true"></i>
										</small>
									</a>
     		 						<?php 	}	
											else 
											{
									?>	
     		 						<a class="btn btn-sm" style="padding: 0px 0px !important;" href="customers/customers_deactive/<?php echo $res->id; ?>">
										<small class="label pull-right bg-red" style="padding: 8px;">
											<i class="fa fa-thumbs-down" aria-hidden="true"></i>
										</small>
									</a>
									<?php } ?>
								</td>
     		 				</tr>	
     		 				<?php } ?>
     		 			</tbody>
				</table>		
				
			</div>
		</div>
	</div>
</div>					