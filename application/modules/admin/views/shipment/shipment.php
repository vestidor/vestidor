<?php $this->layout('layouts::default') ?>
<?php
$ci=&get_instance();
$ci->load->helper('other');
?> 
<?php 
	$ci =& get_instance();
	$ci->load->library('session');
/*	print_r($ci->session->userdata());
	echo"<br>"; */
	$x=$ci->session->userdata('user_group');
	$y=$ci->session->userdata();
	$users= $y['user_id'];
?>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.0/js/responsive.bootstrap.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
<style>
.dataTables_paginate.paging_simple_numbers
{
	text-align: right;
}
.pagination
{
	margin: 0px;
}
.dataTables_filter
{
	text-align: right;
}
#example_wrapper
{
	margin-top: 20px;
}
</style>
<div class="row">
	<div class="col-xs-12">
    	<div class="box">
            <div class="box-header">
              	<h3 class="box-title">Shipment Listing</h3>
                <hr />			
				<table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">	
  						<thead>
    						<tr>
    							<th>Customer Name</th>
    							<th>Customer Email</th>
    							<th>Created Date</th>
    							<th>Order Date</th>
    							<th>Withdraw Date</th>	
      							<th>Status</th>
      							<th>Action</th>
    						</tr>
  						</thead>
  						<tfoot>
  							<tr>
    							<th>Customer Name</th>
    							<th>Customer Email</th>
    							<th>Created Date</th>
    							<th>Order Date</th>
    							<th>Withdraw Date</th>	
      							<th>Status</th>
      							<th>Action</th>
    						</tr>	
  						</tfoot>  	
  						<tbody>
  						<?php foreach($result as $res) { ?>	
    						<tr>
    							<?php $user = get_user($res->userID) ?>
    							<?php foreach($user as $u) { ?>	
    							<td><?php echo $u->username; ?></td>
    							<td><?php echo $u->email; ?></td>
    							<?php } ?>
    							<td><?php echo date('d-m-Y' ,strtotime($res->createdOn)); ?></td>
    							<td><?php if($res->orderDate != '0000-00-00') { echo date('d-m-Y' ,strtotime($res->orderDate)); } else { echo "--"; } ?></td>
     		 					<td><?php if($res->withdrawDate != '0000-00-00') { echo date('d-m-Y' ,strtotime($res->withdrawDate)); } else { echo "--"; } ?></td>
     		 					<td><?php 
     		 							if($res->status == 1){ 
     		 								echo "In Process";
										} elseif($res->status == 2) {
											echo "In Delivery";
										} else {
											echo "delivered";
										}
     		 						?>
     		 					</td>
     		 					<td><?php
     		 							if($res->status == 1){ ?>
     		 								<a href="shipment/delivery/<?php echo $res->ID; ?>" class="btn btn-info btn-xs">Delivery</a>
     		 							<?php	
										} elseif($res->status == 2) { ?>
											<a href="shipment/delivered/<?php echo $res->ID; ?>" class="btn btn-success btn-xs">Delivered</a>
										<?php	
										} else {
											echo "delivered";
										}
										?>		
     		 					</td>
     		 				</tr>
     		 			<?php } ?>	
     		 			</tbody>
				</table>		
				
			</div>
		</div>
	</div>
</div>					