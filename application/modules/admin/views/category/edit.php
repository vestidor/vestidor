<?php $this->layout('layouts::default') ?>
<?php 
	$ci =& get_instance();
	$ci->load->library('session');
	$x=$ci->session->userdata('user_group');
	$y=$ci->session->userdata();
	$users= $y['user_id'];
?>
<div class="row">
	<div class="col-xs-12">
    	<div class="box">
            <div class="box-header">
              	<h3 class="box-title">Category Edit</h3>
              	<hr />
              	
              	<form action="category/update" method="post" accept-charset="utf-8" enctype="multipart/form-data">
				<?php foreach($result as $res) {  ?>
					<input type="hidden" name="id" value="<?php echo $res->ID; ?>" />
					<input type="hidden" name="aid" value="<?php echo $res->adminID; ?>" />	
				<div class="form-group col-md-12">
					<label for="email">CategoryName Name</label>
					<input type="text" name="category" value="<?php echo $res->categoryName; ?>" class="form-control input-xs" />
				</div>
				<?php } ?>
				<div class="form-group col-md-12">
					<input type="submit" name="add" value="Add" class="btn btn-primary" />
				</div>	
              		
              	</form>	
			</div>
		</div>
	</div>
</div>					