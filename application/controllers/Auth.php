<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	
		
		
		// CI Bootstrap libraries
		$this->load->library('form_builder');
		$this->load->library('system_message');
		$this->load->library('email_client');
		 $this->load->library('session');
		$this->push_breadcrumb('Auth');
		$this->mViewData['enable_breadcrumb'] = TRUE;
		$user_info=$this->session->userdata();
	}

	/**
	 * Registration page
	 */
	public function sign_up()
	{
		$form = $this->form_builder->create_form();

		if ($form->validate())
		{
			// passed validation
			$identity = $this->input->post('email');
			$password = $this->input->post('password');
			$additional_data = array(
				'username'	=> $this->input->post('username'),
			);
			$group = array('group'=> 4);

			// create user (default group as "members")
			$user = $this->ion_auth->register($identity, $password, $identity, $additional_data, $group);
			$id = $user['id'];
			$acti = $user['activation'];
			echo $id;
			echo $acti;

			if ($user)
			{	
				// send email using Email Client library
				if ($this->config->item('email_activation', 'ion_auth') && !$this->config->item('use_ci_email', 'ion_auth'))
				{
					$subject = $this->lang->line('email_activation_subject');
					$email_view = $this->config->item('email_templates', 'ion_auth').$this->config->item('email_activate', 'ion_auth');
					//$this->email_client->send($identity, $subject, $email_view, $user);
					$url = base_url();
    				$subject = "Welcome to Vestidor";
					$message = "<b>You have been successfully Registered vestidor user.</b><br/><a href='$url/auth/activate/$id/$acti'>Activate Your Account</a>";
					$from = 'info@vestidor.com';
					$headers = "MIME-Version: 1.0" . "\r\n";
					$headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
					$headers .= "From: " . $from . "\r\n";
					$headers .= "Reply-To: " . $from . "\r\n";
					$headers .= "X-Mailer: PHP/" . phpversion();
					$headers .= "X-Priority: 1" . "\r\n";
					$retval = mail($identity, $subject, $message, $headers);	
				}

				// success
				$messages = $this->ion_auth->messages();
				$this->system_message->set_success($messages);
				redirect('login');
			}
			else
			{
				// failed
				$errors = $this->ion_auth->errors();
				$this->system_message->set_error($errors);
				refresh();
			}
		}

		// display form
		$this->mViewData['form'] = $form;
		$this->render('auth/sign_up');
	}
	
	public function sign_up_step_1()
	{ 
		$this->render('auth/sign-up-step-1');
	}
	/*  new sign Up */
	
	public function sign_up_customer()
	{
		$identity = $this->input->post('email');
		$password = $this->input->post('password');
		
		$additional_data = array(
				'username'	=> $this->input->post('name'),	
				'surname'	=> $this->input->post('surname'),
				'phone'	=> $this->input->post('phone'),
				'dob'	=> date ("Y-m-d",strtotime($this->input->post('dob'))),
				'gender'	=> $this->input->post('gender'),
				'province'	=> $this->input->post('province'),
				'zipCode'	=> $this->input->post('zipcode'),
				'city'	=> $this->input->post('city'),
				'height'	=> $this->input->post('height'),
				'weight'	=> $this->input->post('weight'),
				'eyeColor'	=> $this->input->post('eyecolor'),
				'hairColor'	=> $this->input->post('hairColor'),
				'shirtSize'	=> $this->input->post('shirtSize'),
				'neck'	=> $this->input->post('neck'),
				'sleeveLength'	=> $this->input->post('sleeveLength'),
				'jacket'	=> $this->input->post('jacket'),
				'shoes'	=> $this->input->post('shoes'),
				'trousers'	=> $this->input->post('trousers'),
				'inseam'	=> $this->input->post('inseam'),
				'dress'	=> $this->input->post('dress'),
				'bust'	=> $this->input->post('bust'),	
				'band'	=> $this->input->post('band'),
				'cup'	=> $this->input->post('cup'),
				'shipStreet'	=> $this->input->post('deliveryStreet'),
				'shipNumber'	=> $this->input->post('deliveryNumber'),
				'shipFloor'	=> $this->input->post('deliveryFloor'),
				'shipApartment'	=> $this->input->post('deliveryApartment'),
				'shipProvince'	=> $this->input->post('deliveryProvince'),
				'shipZipcode'	=> $this->input->post('deliveryZipCode'),
				'shipCity'	=> $this->input->post('deliveryCity'),
				'type'	=> $this->input->post('deliveryType')
		);
		$group = array('group'=> 4);

			// create user (default group as "members")
			$user = $this->ion_auth->register($identity, $password, $identity, $additional_data, $group);
			$id = $user['id'];
			$acti = $user['activation'];
		
			if ($user)
			{	
				// send email using Email Client library
				if ($this->config->item('email_activation', 'ion_auth') && !$this->config->item('use_ci_email', 'ion_auth'))
				{
					$subject = $this->lang->line('email_activation_subject');
					$email_view = $this->config->item('email_templates', 'ion_auth').$this->config->item('email_activate', 'ion_auth');
					//$this->email_client->send($identity, $subject, $email_view, $user);
					$url = base_url();
    				$subject = "Welcome to Vestidor";
					$message = "<b>You have been successfully Registered vestidor user.</b><br/><a href='$url/auth/activate/$id/$acti'>Activate Your Account</a>";
					$from = 'info@vestidor.com';
					$headers = "MIME-Version: 1.0" . "\r\n";
					$headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
					$headers .= "From: " . $from . "\r\n";
					$headers .= "Reply-To: " . $from . "\r\n";
					$headers .= "X-Mailer: PHP/" . phpversion();
					$headers .= "X-Priority: 1" . "\r\n";
					$retval = mail($identity, $subject, $message, $headers);	
					
				}

				// success
				$messages = $this->ion_auth->messages();
				$this->system_message->set_success($messages);
				redirect('login');
			}
			else
			{
				// failed
				$errors = $this->ion_auth->errors();
				$this->system_message->set_error($errors);
				refresh();
			}
		
	
	
	}
	/**
	 * Login page
	 */
	public function login()
	{
		$form = $this->form_builder->create_form();

		if ($form->validate())
		{
			// passed validation
			$identity = $this->input->post('email');
			$password = $this->input->post('password');
			$remember = ($this->input->post('remember')=='on');
			
			if ($this->ion_auth->login($identity, $password, $remember))
			{
				// success
				$messages = $this->ion_auth->messages();
				$this->system_message->set_success($messages);

				// redirect to user dashboard
				redirect('account');
			}
			else
			{
				// failed
				$errors = $this->ion_auth->errors();
				$this->system_message->set_error($errors);
				refresh();
			}
		}

		// display form
		$this->mViewData['form'] = $form;
		$this->render('auth/login');
	}

	/**
	 * Logout
	 */
	public function logout()
	{
		$this->ion_auth->logout();	
		redirect();
	}

	/**
	 * Activation
	 */
	public function activate($id = NULL, $code = NULL)
	{
		if ( empty($id) )
		{
			redirect();
		}
		else if ( !empty($code) )
		{
			$activation = $this->ion_auth->activate($id, $code);
		}
		else if ($this->ion_auth->is_admin())
		{
			$activation = $this->ion_auth->activate($id);
		}

		if ($activation)
		{
			// success
			$messages = $this->ion_auth->messages();
			$this->system_message->set_success($messages);
			redirect('login');
		}
		else
		{
			// failed
			$errors = $this->ion_auth->errors();
			$this->system_message->set_error($errors);
			redirect('auth/forgot_password');
		}
	}

	/**
	 * Forgot Password page
	 */
	public function forgot_password()
	{
		$form = $this->form_builder->create_form();

		if ($form->validate())
		{
			// passed validation
			$identity = $this->input->post('email');
			$user = $this->ion_auth->forgotten_password($identity);
			$forpass = $user['forgotten_password_code'];
			if ($user)
			{
				if (!$this->config->item('use_ci_email', 'ion_auth'))
				{
					// send email using Email Client library
					$subject = $this->lang->line('email_forgotten_password_subject');
					$email_view = $this->config->item('email_templates', 'ion_auth').$this->config->item('email_forgot_password', 'ion_auth');
					//$this->email_client->send($identity, $subject, $email_view, $user);
					$url = base_url();					
					$message = "<b>Reset Password for $identity</b>Please click this link to <br/><a href='$url/auth/reset_password/$forpass'>Reset Your Password</a>";
					$from = 'info@vestidor.com';
					$headers = "MIME-Version: 1.0" . "\r\n";
					$headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
					$headers .= "From: " . $from . "\r\n";
					$headers .= "Reply-To: " . $from . "\r\n";
					$headers .= "X-Mailer: PHP/" . phpversion();
					$headers .= "X-Priority: 1" . "\r\n";
					$retval = mail($identity, $subject, $message, $headers);	
						
				}

				// success
				$messages = $this->ion_auth->messages();
				$this->system_message->set_success($messages);
				redirect('login');
			}
			else
			{
				// failed
				$errors = $this->ion_auth->errors();
				$this->system_message->set_error($errors);
				refresh();
			}
		}

		// display form
		$this->mViewData['form'] = $form;
		$this->render('auth/forgot_password');
	}

	/**
	 * Reset Password page
	 */
	public function reset_password($code = NULL)
	{
		if (!$code)
		{
			redirect();
		}

		// check whether code is valid
		$user = $this->ion_auth->forgotten_password_check($code);

		if ($user)
		{
			$form = $this->form_builder->create_form();

			if ($form->validate())
			{
				// passed validation
				$identity = $user->email;
				$password = $this->input->post('password');
				
				// confirm update password
				if ( $this->ion_auth->reset_password($identity, $password) )
				{
					if (!$this->config->item('use_ci_email', 'ion_auth'))
					{
						// send email using Email Client library
						$subject = $this->lang->line('email_new_password_subject');
						$email_view = $this->config->item('email_templates', 'ion_auth').$this->config->item('email_forgot_password_complete', 'ion_auth');
						
						//$data = array('identity' => $identity);
						//$this->email_client->send($identity, $subject, $email_view, $data);
						
						$message = "New Password for $identity Your password has been reset to: $password";
						$from = 'info@vestidor.com';
						$headers = "MIME-Version: 1.0" . "\r\n";
						$headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
						$headers .= "From: " . $from . "\r\n";
						$headers .= "Reply-To: " . $from . "\r\n";
						$headers .= "X-Mailer: PHP/" . phpversion();
						$headers .= "X-Priority: 1" . "\r\n";
						$retval = mail($identity, $subject, $message, $headers);	
					}

					// success
					$messages = $this->ion_auth->messages();
					$this->system_message->set_success($messages);
					redirect('login');
				}
				else
				{
					// failed
					$errors = $this->ion_auth->errors();
					$this->system_message->set_error($errors);
					redirect('auth/reset_password/' . $code);
				}
			}

			// display form
			$this->mViewData['form'] = $form;
			$this->render('auth/reset_password');
		}
		else
		{
			// code invalid
			$errors = $this->ion_auth->errors();
			$this->system_message->set_error($errors);
			redirect('auth/forgot_password', 'refresh');
		}
	}

	public function delete()
	{
		$ci =& get_instance();
		$ci->load->library('session');
		$x=$ci->session->userdata('user_group');
		$y=$ci->session->userdata();
		$users= $y['user_id'];
		
		$email = $y['identity'];
		
		$url = base_url();	
		$subject = "Delete Account";				
		$message = "<b>Delete Account for $email</b>Please click this link to <br/><a href='$url/auth/delete_account/$users'>Delete Your Account</a>";
		$from = 'info@vestidor.com';
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
		$headers .= "From: " . $from . "\r\n";
		$headers .= "Reply-To: " . $from . "\r\n";
		$headers .= "X-Mailer: PHP/" . phpversion();
		$headers .= "X-Priority: 1" . "\r\n";
		$retval = mail($email, $subject, $message, $headers);	
		
		$this->ion_auth->logout();	
		$this->session->set_flashdata('delete', 'Delete Account Email Send');
		redirect('login');
		
	}
	
	public function delete_account($id)
	{
		$data = $this->ion_auth->delete_account($id);
		redirect('login');
	}
	
	public function images()
	{
		$image = $this->input->post('image');
		$this->load->helper(array('form', 'url')); 
		$this->load->helper('file');
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['file_name'] = date('YmdHisu').rand(0,100);
	 	$this->load->library('upload',$config);
		if($this->upload->do_upload($image))
		{
			echo "done";
		}
		else
		{
			$error = array('error' => $this->upload->display_errors());
			 
			 print_r($error);
			echo "Error";
		}

	}
} 
