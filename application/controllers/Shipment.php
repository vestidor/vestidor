<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shipment extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		// only login users can access Account controller
		//$y=$this->session->userdata(); $y['user_id'];
		//$this->verify_auth();
		
		if(!$this->ion_auth->logged_in())
		{
			redirect('login');
		}
	}

	public function index()
	{
		$ci = &get_instance();
		$ci -> load -> library('session');
		$x = $ci -> session -> userdata('user_group');
		$y = $ci -> session -> userdata();
		$users = $y['user_id'];
		$this->load->model('shipment_model');
		$data=$this->shipment_model->get_shipment_by_id($users);
		$datatwo=$this->shipment_model->get_shipment_by_stylist($users);
		$this->mViewData['stylistdata']=$datatwo;
		$this->mViewData['result']=$data;
		$this->render('shipment/shipment');
		
	}

}
?>