<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		// only login users can access Account controller
		//$y=$this->session->userdata(); $y['user_id'];
		//$this->verify_auth();
		
		if(!$this->ion_auth->logged_in())
		{
			redirect('login');
		}
	}

	public function index()
	{
		$ci = &get_instance();
		$ci -> load -> library('session');
		$x = $ci -> session -> userdata('user_group');
		$y = $ci -> session -> userdata();
		$users = $y['user_id'];
		$this->load->model('account_model');
		$data=$this->account_model->get_by_id($users);
		$datatwo=$this->account_model->get_stylist_by_id($users);
		$this->mViewData['stylist']=$datatwo;
		$this->mViewData['result']=$data;
		$this->render('account/account');
		
	}
	
	public function edit($id)
	{
		$this->load->model('account_model');
		$data=$this->account_model->get_by_id($id);
		$datatwo=$this->account_model->get_stylist_by_id($id);
		$this->mViewData['stylist']=$datatwo;
		$this->mViewData['result']=$data;
		$this->render('account/edit');
	}
	
	public function update()
	{
		$id = $this->input->post('id');
		$data['ID'] = $this->input->post('id');
		$data['phoneOne'] = $this->input->post('phoneOne');
		$data['phoneTwo'] = $this->input->post('phoneTwo');
		$data['officeAddress'] = $this->input->post('officeAddress');
		$data['homeAddress'] = $this->input->post('homeAddress');
		$data['officeZip'] = $this->input->post('officeZip');
		$data['homeZip'] = $this->input->post('homeZip');
		$data['gender'] = $this->input->post('gender');
		$data['birthDate'] = date ("Y-m-d",strtotime($this->input->post('birthDate')));
		$data['bodyHeight'] = $this->input->post('bodyHeight');
		$data['bodyWeight'] = $this->input->post('bodyWeight');
		$data['sizeShirt'] = $this->input->post('sizeShirt');
		$data['jeansWidth'] = $this->input->post('jeansWidth');
		$data['jeansHeight'] = $this->input->post('jeansHeight');
		$data['shoeSize'] = $this->input->post('shoeSize');
		$data['withdrawalAddress'] = $this->input->post('withdrawalAddress');
		
		$this->load->model('account_model');
		$id=$this->account_model->update($data,$id);
		redirect('account');
	}

	public function update_stylist()
	{
		$id = $this->input->post('id');
		$data['ID'] = $this->input->post('id');
		$data['phoneOne'] = $this->input->post('phoneOne');
		$data['phoneTwo'] = $this->input->post('phoneTwo');
		$data['address'] = $this->input->post('address');
		
		$stylist['username'] = $this->input->post('username');
		$stylist['email'] = $this->input->post('email');
		
		$this->load->model('account_model');
		$id=$this->account_model->update_stylist($data,$id,$stylist);
		redirect('account');
	}

}