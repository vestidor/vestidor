<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		// only login users can access Account controller
		//$y=$this->session->userdata(); $y['user_id'];
		//$this->verify_auth();
		
		if(!$this->ion_auth->logged_in())
		{
			redirect('login');
		}
	}

	public function index($id)
	{
		$this->load->model('products_model');
		$data=$this->products_model->get_products();
		$datatwo=$this->products_model->get_box($id);
		$this->mViewData['box']=$datatwo;
		$this->mViewData['products']=$data;
		$this->render('products/products');	
	}
	
}
?>