<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Addproduct extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		// only login users can access Account controller
		//$y=$this->session->userdata(); $y['user_id'];
		//$this->verify_auth();
		
		if(!$this->ion_auth->logged_in())
		{
			redirect('login');
		}
	}

	public function index()
	{
		$data = $_POST;
		
		$id = $this->input->post('boxID');
		$pid = $this->input->post('product');

		$this->load->model('products_model');
		$data=$this->products_model->add_product($id,$pid);
		redirect('products/'.$id);
	}
	
	public function delete($id)
	{
		$this->load->model('products_model');
		$ids = $this->products_model->select_product_id($id);
		foreach($ids as $i)
		{
			$ii = $i->boxID;
		}
		$data=$this->products_model->delete_product($id);
		redirect('products/'.$ii);
	}
	
}
?>