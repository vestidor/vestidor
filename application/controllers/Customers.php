<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customers extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		// only login users can access Account controller
		//$y=$this->session->userdata(); $y['user_id'];
		//$this->verify_auth();
		
		if(!$this->ion_auth->logged_in())
		{
			redirect('login');
		}
	}

	public function index($id)
	{
		$this->load->model('customers_model');
		$data=$this->customers_model->get_by_stylist($id);
		$this->mViewData['result']=$data;
		$this->render('customers/customers');	
	}
	
}
?>