<?php 
defined('BASEPATH') OR exit ('No direct script allowed');

class Order extends MY_Controller{
	
	public function __construct()
	{
		parent::__construct();

		// only login users can access Account controller
		//$y=$this->session->userdata(); $y['user_id'];
		//$this->verify_auth();
		
		if(!$this->ion_auth->logged_in())
		{
			redirect('login');
		}
	}
	
	public function index()
	{
		$ci = &get_instance();
		$ci -> load -> library('session');
		$x = $ci -> session -> userdata('user_group');
		$y = $ci -> session -> userdata();
		$users = $y['user_id'];
		$this->load->model('order_model');
		$data=$this->order_model->get_box_id($users);
		$this->mViewData['result']=$data;
		$this->render('order/order');
	}
	public function check($id)
	{
		$this->load->model('order_model');
		$data=$this->order_model->get_boxby_id($id);
		$this->mViewData['results']=$data;
		$this->mViewData['ii']=$id;
		$this->render('order/check');
	}
	public function delete($id)
	{
		$this->load->model('products_model');
		$ids = $this->products_model->select_product_id($id);
		foreach($ids as $i)
		{
			$ii = $i->boxID;
		}
		$data=$this->products_model->delete_product($id);
		redirect('order/check/'.$ii);
	}
	public function confirm()
	{
		$total = $this->input->post('price');
		$this->mViewData['ii']= $this->input->post('boxid');
		$this->mViewData['price']= $total;
		$this->render('order/confirm');
	}
	public function shipment()
	{
		$address = $this->input->post('address');
		$boxID = $this->input->post('boxID');
		$userID = $this->input->post('userID');
		$price = $this->input->post('price');
		
		$this->load->model('order_model');
		$data=$this->order_model->insert_order($address,$boxID,$userID,$price);
		redirect('order');
	}
	
	public function finalorder()
	{
		$ci = &get_instance();
		$ci -> load -> library('session');
		$x = $ci -> session -> userdata('user_group');
		$y = $ci -> session -> userdata();
		$users = $y['user_id'];
		$boxid = $this->input->post('boxID');
		$price = $this->input->post('price');
		$this->load->model('order_model');
		$data=$this->order_model->insert_finalorder($boxid,$price,$users);
		redirect('order');
	}
	
}
