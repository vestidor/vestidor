<?php 
defined('BASEPATH') OR EXIT ('No direct script access allowed');

class Box extends MY_Controller {
		
	public function __construct()
	{
		parent::__construct();

		// only login users can access Account controller
		//$y=$this->session->userdata(); $y['user_id'];
		//$this->verify_auth();
		
		if(!$this->ion_auth->logged_in())
		{
			redirect('login');
		}
	}	
	
	
	public function index($id)
	{
		$ci = &get_instance();
		$ci -> load -> library('session');
		$x = $ci -> session -> userdata('user_group');
		$y = $ci -> session -> userdata();
		$users = $y['user_id'];
		$this->load->model('box_model');
		$da=$this->box_model->check_box($users);
		if($da == 0)
		{	
			$data['userID'] = $users;
			$data['stylistID'] = $id;
			$data['createdOn'] = date('Y-m-d h:i:s');
			$this->load->model('box_model');
			$data=$this->box_model->insert_box($data);
			$this->session->set_flashdata('job_post_success','You Box Order successfully Send');
			redirect('account');
		}
		else 
		{
			$this->session->set_flashdata('job_post','You Box Order Already Send');
			redirect('account');
		}
	}
}
?>