<?php $this->layout('layouts::default') ?>
<?php 
	$ci =& get_instance();
	$ci->load->library('session');
	$x=$ci->session->userdata('user_group');
	$y=$ci->session->userdata();
	$users= $y['user_id'];
?>
<div class="row">
	<div class="col-md-10 text-right">
			<a href="account" class="btn btn-primary">Account</a>
			<a href="logout" class="btn btn-danger">LogOut</a>
		</div>	
	<div class="col-md-12">
		<div class="col-md-8">
			<?php foreach($result as $r) { ?>
				<div class="col-md-12 text-center">
					<h1>Hello! I am <?php echo $r->username; ?>, your stylist!</h1>
				</div>
				<div class="col-md-6">
					<img src="<?php echo base_url() ?>uploads/71.jpg" />
				</div>
				 
				<div class="col-md-6">
					<div class="col-md-12 text-uppercase">
						<h1><?php echo $r->username; ?></h1>
						<hr />
					</div>
					<div class="col-md-12">
						<i class="fa-li fa fa-envelope" style="font-size:16px;"></i>
						<a href="mailto:<?php echo $r->email; ?>"><?php echo $r->email; ?></a>
					</div>
					<div class="col-md-12">
						<i class="fa-li fa fa-mobile-phone" style="font-size:16px;"></i>
						<?php echo $r->phoneOne; ?> - <?php echo $r->phoneTwo; ?>
					</div>
				</div>
				
			<?php } ?>	
		</div>
	</div>
	
</div>

