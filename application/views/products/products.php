<?php $this->layout('layouts::default') ?>
<?php
$ci=&get_instance();
$ci->load->helper('common_helper');
?>
<?php 
	$ci =& get_instance();
	$ci->load->library('session');
	$x=$ci->session->userdata('user_group');
	$y=$ci->session->userdata();
	$users= $y['user_id'];
	$email= $y['email'];
?>
<div class="row">
	<div class="col-md-12">
		<h1>products</h1>
		<hr />
	</div>
	<div class="col-md-10 text-right">
			<a href="account" class="btn btn-primary">Account</a>
			<a href="logout" class="btn btn-danger">LogOut</a>
		</div>	
	<div class="col-md-12">
			<div class="col-md-6" style="border: 1px solid;">
					<?php foreach ($products as $p) { ?>
					<div class="col-md-12">	
					<form method="post" enctype="multipart/form-data" action="addproduct">	
					<?php foreach ($box as $b) { ?>
					<?php $boxid = $b->ID; ?>
					<input type="hidden" name="boxID" value="<?php echo $b->ID; ?>" />
					<input type="hidden" name="product" value="<?php echo $p->ID ?>" />
					<?php } ?>	
					<div class="col-md-3">
						<h5><?php echo $p->name; ?></h5>
					</div>
					<div class="col-md-1">
						<?php echo $p->simplePrice; ?>
					</div>
					<div class="col-md-1">
						<?php echo $p->discountPrice; ?>
					</div>
					<div class="col-md-2">
						<?php $brand = get_brand(); foreach($brand as $br) { if($p->brandName == $br->ID ) { echo $br->brandName;  } } ?>	
					</div>
					<div class="col-md-2">
						<?php $category = get_category(); foreach($category as $cat) { if($p->category == $cat->ID) { echo $cat->categoryName; } } ?>
					</div>
					<div class="col-md-2">
						<img src="<?php echo base_url(); ?>uploads/<?php echo $p->image ?>" style="width: 50%;" />
					</div>
					<div class="col-md-1">
						<input type="submit" name="submit" value="Add" class="btn btn-success" />
					</div>
					</form> 
					</div>
					<?php } ?>	
			</div>
			<div class="col-md-6" style="border: 1px solid">
				<h1><?php echo $email; ?> Products of Box</h1>
				<?php	$boxdata = get_box($boxid);
						foreach($boxdata as $bd) { 
						 $result = get_product($bd->productID); 
						 foreach($result as $r) {?> 
						 <div class="col-md-12">	
						<div class="col-md-3">
							<h5><?php echo $r->name; ?></h5>
						</div>
						<div class="col-md-1">
							<?php echo $r->simplePrice; ?>
						</div>
						<div class="col-md-1">
							<?php echo $r->discountPrice; ?>
						</div>
						<div class="col-md-2">
							<?php $brand = get_brand(); foreach($brand as $br) { if($r->brandName == $br->ID ) { echo $br->brandName;  } } ?>	
						</div>
						<div class="col-md-2">
							<?php $category = get_category(); foreach($category as $cat) { if($r->category == $cat->ID) { echo $cat->categoryName; } } ?>
						</div>
						<div class="col-md-2">
							<img src="<?php echo base_url(); ?>uploads/<?php echo $r->image ?>" style="width: 50%;" />
						</div>
						<div class="col-md-1">	
							<a href="addproduct/delete/<?php echo $bd->ID; ?>" class="btn btn-danger">Delete</a>
						</div>	
						</div>
				<?php } } ?>
			</div>
		</div>


</div>