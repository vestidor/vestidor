<?php $this->layout('layouts::default') ?>
<?php
$ci=&get_instance();
$ci->load->helper('common_helper');
?>
<?php 
	$ci =& get_instance();
	$ci->load->library('session');
	$x=$ci->session->userdata('user_group');
	$y=$ci->session->userdata();
	$users= $y['user_id'];
?>
<div class="row">
	<div class="col-md-12">
		<h1>My Orders</h1>
		<hr />
	</div>
	<div class="col-md-10 text-right">
			<a href="account" class="btn btn-primary">Account</a>
			<a href="logout" class="btn btn-danger">LogOut</a>
		</div>	
	<div class="col-md-12">
		<div class="col-md-2">&nbsp;</div>
		<div class="col-md-8" style="border-bottom: 1px solid;">
			<h1>Products of Box</h1>
				<?php	
						$total=0;
						
						foreach($results as $bd) {
						 $result = get_product($bd->productID); 
						 foreach($result as $r) {    $total+= $r->simplePrice; ?> 
						 <div class="col-md-12">	
						<div class="col-md-3">
							<h5><?php echo $r->name; ?></h5>
						</div>
						<div class="col-md-1">
							<?php echo $r->simplePrice; ?>
						</div>
						<div class="col-md-1">
							<?php echo $r->discountPrice; ?>
						</div>
						<div class="col-md-2">
							<?php $brand = get_brand(); foreach($brand as $br) { if($r->brandName == $br->ID ) { echo $br->brandName;  } } ?>	
						</div>
						<div class="col-md-2">
							<?php $category = get_category(); foreach($category as $cat) { if($r->category == $cat->ID) { echo $cat->categoryName; } } ?>
						</div>
						<div class="col-md-2">
							<img src="<?php echo base_url(); ?>uploads/<?php echo $r->image ?>" style="width: 50%;" />
						</div>
						<?php } ?>
						<div class="col-md-1">	
							<a href="order/delete/<?php echo $bd->ID; ?>" class="btn btn-danger">Delete</a>
						</div>	
						</div>
				<?php }  ?>
				
						<div class="col-md-12 text-right">
							<h5>Total Price:- <?php echo $total; ?></h5>
						</div>	
						
						
						<?php $shipment = get_shipment($ii); ?>
						<?php if($shipment == 0) { ?>
						<div class="col-md-12">
							<form action="order/confirm" method="post" enctype="multipart/form-data">
								<input type="hidden" value="<?php echo $ii; ?>" name="boxid" />
								<input type="hidden" value="<?php echo $total ?>" name="price" />
								<input type="submit" value="Order" name="submit" class="btn btn-success" />								
							</form>
							
						</div>		
						<?php } ?>
						
						<?php $status =get_shipmentstatus($ii); ?>
						<?php foreach($status as $s) { ?>
						<?php if($s->status == 3) { ?>	
						<div class="col-md-12 text-right">
							<form action="order/finalorder" method="post" enctype="multipart/form-data">
								<input type="hidden" name="boxID" value="<?php echo $ii; ?>" />
								<input type="hidden" name="price" value="<?php echo $total; ?>" />
								<input type="submit" name="submit" value="Finalize" class="btn btn-success" />
							</form>
						</div>	
						<?php } } ?>	
		
		</div>
	</div>

</div>