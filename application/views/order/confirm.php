<?php $this->layout('layouts::default') ?>
<?php
$ci=&get_instance();
$ci->load->helper('common_helper');
?>
<?php 
	$ci =& get_instance();
	$ci->load->library('session');
	$x=$ci->session->userdata('user_group');
	$y=$ci->session->userdata();
	$users= $y['user_id'];
?>
<div class="row">
	<div class="col-md-12">
		<h1>My Orders</h1>
		<hr />
	</div>
	<div class="col-md-10 text-right">
			<a href="account" class="btn btn-primary">Account</a>
			<a href="logout" class="btn btn-danger">LogOut</a>
		</div>	
	<div class="col-md-12">
		<div class="col-md-2">&nbsp;</div>
		<div class="col-md-8" style="border-bottom: 1px solid;">
			<h1>Order of Box</h1>	
			<hr />
			
			
			<form method="post" enctype="multipart/form-data" action="order/shipment">	
				<input type="hidden" name="boxID" value="<?php echo $ii; ?>" />
				<input type="hidden" name="userID" value="<?php echo $users; ?>" />
				<input type="hidden" name="price" value="<?php echo $price; ?>" />
				<?php $resu = get_useraddress($users); ?>
				<?php foreach($resu as $res) { ?>
					<div class="col-md-12">
						<input type="radio" name="address" value="1" />
						<?php echo $res->officeAddress.",".$res->officeZip; ?>
					</div>
					<div class="col-md-12">
						<input type="radio" name="address" value="2" />
						<?php echo $res->homeAddress.",".$res->homeZip; ?>
					</div>
				<?php } ?>	
				<input type="submit" value="Confirm" name="confirm" class="btn btn-danger" />
			</form>		
		</div>
		
		
	</div>

</div>