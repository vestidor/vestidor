<?php $this->layout('layouts::default') ?>
<?php 
	$ci =& get_instance();
	$ci->load->library('session');
	$x=$ci->session->userdata('user_group');
	$y=$ci->session->userdata();
	$users= $y['user_id'];
?>
<div class="row">
	<div class="col-md-12">
		<h1>My Orders</h1>
		<hr />
	</div>
	<div class="col-md-10 text-right">
			<a href="account" class="btn btn-primary">Account</a>
			<a href="logout" class="btn btn-danger">LogOut</a>
		</div>	
	<div class="col-md-12">
		<div class="col-md-2">&nbsp;</div>
		<div class="col-md-8" style="border-bottom: 1px solid;">
			<table>
				<tr>
					<td style="padding: 5px;">Request Date </td>
					<td style="padding: 5px;">Order Date</td>
					<td style="padding: 5px;">Withdraw Date</td>
					<td style="padding: 5px;">Status</td>
				</tr>
				<?php foreach($result as $r) { ?>
				<tr>
					<td style="padding: 5px;"><?php echo date('d-m-Y' ,strtotime($r->createdOn)); ?></td>
					<td style="padding: 5px;">
						<?php if($r->orderDate != '0000-00-00') { echo date('d-m-Y' ,strtotime($r->orderDate)); } else { echo "--"; } ?>
					</td>
					<td style="padding: 5px;">
						<?php if($r->withdrawDate != '0000-00-00') { echo date('d-m-Y' ,strtotime($r->withdrawDate)); } else { echo "--"; } ?>
					</td>
					<td style="padding: 5px;">
						
						<?php if($r->boxStatus == '0') { ?>
							<a href="order/check/<?php echo $r->ID; ?>">Assemble Products</a>
						<?php } else { ?>
								Completed
						<?php } ?>		
					</td>
				</tr>
				<?php } ?>
				
			</table>
		</div>
	</div>

</div>