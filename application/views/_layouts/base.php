<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">
<head>
<meta charset="utf-8">

	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>DESNUDIO</title>
	
	<link rel="stylesheet" type="text/css" href="<?php echo dist_url('vertidor/css/bootstrap.min.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo dist_url('vertidor/css/font-awesome.min.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo dist_url('vertidor/css/owl.carousel.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo dist_url('vertidor/css/chosen.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo dist_url('vertidor/css/lightbox.min.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo dist_url('vertidor/css/pe-icon-7-stroke.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo dist_url('vertidor/css/jquery.mCustomScrollbar.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo dist_url('vertidor/css/magnific-popup.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo dist_url('vertidor/css/style.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo dist_url('vertidor/css/custom.css'); ?>">
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Montserrat">
	<link href='https://fonts.googleapis.com/css?family=Playfair+Display:400italic,400,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Roboto:300,100,100italic,300italic,400,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
	<link href="https://fonts.googleapis.com/css?family=Allura" rel="stylesheet">

		<!-- CSS files for plugins -->
	<link href="<?php echo dist_url('vertidor/css/animate.css'); ?>" rel="stylesheet">	
</head>

<body id="top">
	<?php // Main content (from inner view, or nested layout) ?>
	<?=$this->section('content')?>
	
		<script type="text/javascript" src="<?php echo dist_url('vertidor/js/jquery-2.1.4.min.js'); ?>"></script>
		<script type="text/javascript" src="<?php echo dist_url('vertidor/js/bootstrap.min.js'); ?>"></script>
		<script type="text/javascript" src="<?php echo dist_url('vertidor/js/owl.carousel.min.js'); ?>"></script>
		<script type="text/javascript" src="<?php echo dist_url('vertidor/js/chosen.jquery.min.js'); ?>"></script>
		<script type="text/javascript" src="<?php echo dist_url('vertidor/js/Modernizr.js'); ?>"></script>
		<script type="text/javascript" src="<?php echo dist_url('vertidor/js/jquery-ui.min.js'); ?>"></script>
		<script type="text/javascript" src="<?php echo dist_url('vertidor/js/lightbox.min.js'); ?>"></script>
		<script type="text/javascript" src="<?php echo dist_url('vertidor/js/masonry.pkgd.min.js'); ?>"></script>
		<script type="text/javascript" src="<?php echo dist_url('vertidor/js/imagesloaded.pkgd.min.js'); ?>"></script>
		<script type="text/javascript" src="<?php echo dist_url('vertidor/js/isotope.pkgd.min.js'); ?>"></script>
		<script type="text/javascript" src="<?php echo dist_url('vertidor/js/jquery.mCustomScrollbar.concat.min.js'); ?>"></script>
		<script type="text/javascript" src="<?php echo dist_url('vertidor/js/jquery.parallax-1.1.3.js'); ?>"></script>
		<script type="text/javascript" src="<?php echo dist_url('vertidor/js/jquery.magnific-popup.min.js'); ?>"></script>
		<script type="text/javascript" src="<?php echo dist_url('vertidor/js/masonry.js'); ?>"></script>
		<script type="text/javascript" src="<?php echo dist_url('vertidor/js/functions.js'); ?>"></script>

		


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>



	<?=$this->section('scripts_foot')?>

	<?php // Google Analytics ?>
	<?php $this->insert('partials::ga') ?>
</body>
</html>
