<?php $this->layout('layouts::base') ?>
<?php $this->insert('partials::navbar') ?>
<div id="mainContainer" style="margin-top: 155px;">
	<?=$this->section('content')?>

<footer class="footer style2 style4 pb80">
				<div class="footer-top">
					<div class="container">
						<div class="widget contact-info">
						<div class="logo">
						<a href="#"><img alt="" src="<?php echo dist_url('vertidor/images/logos/1.png'); ?>"></a>
						</div>
						</div>
						<ul class="footer-menu">
							<li> <a class="page-scroll" href="<?php echo base_url(); ?>#top">INICIO</a> </li>
							<li> <a class="page-scroll" href="<?php echo base_url(); ?>#about">¿QUÉ ES DESNUDIO?</a> </li>
							<li> <a class="page-scroll" href="<?php echo base_url(); ?>#how">¿CÓMO FUNCIONA?</a> </li>
							<li> <a class="page-scroll" href="<?php echo base_url(); ?>#prod">PRODUCTOS</a> </li>
							<li> <a class="page-scroll" href="<?php echo base_url(); ?>#suscribite">SUSCRÍBETE</a> </li>
						</ul>
					</div>
				</div>
				
			</footer>
	<a href="#" class="scroll_top" title="Scroll to Top" style="display: block;"><i class="fa fa-arrow-up"></i></a>

</div>