<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>
	$(document).ready(function(){
		$(".steptwo").hide();
		$(".stepthree").hide();
		$(".stepfour").hide();
		$(".finalstep").hide();
	});
</script>	
<style>
	#genders
	{
		display: block !important;
	}
	#gender
	{
		display: block !important;
	}
	#genders_chosen
	{
		display: none !important;
	}
	#eyeColors
	{
		display: block !important;
	}
	#eyeColor
	{
		display: block !important;
	}
	#eyeColor_chosen
	{
		display: none !important;
	}
	#hairColors
	{
		display: block !important;
	}
	#hairColor
	{
		display: block !important;
	}
	#hairColor_chosen
	{
		display: none !important;
	}
	#types
	{
		display: block !important;
	}
	#type
	{
		display: block !important;
	}
	#type_chosen
	{
		display: none !important;
	}
	.chosen-container
	{
		display: none !important;
	}
	#deliveryTypes
	{
		display: block !important;
	}
</style>	
<?php $this->layout('layouts::default') ?>


<div id="centerMain" class="stepone">
    <div class="fluid">
    	<div class="progress">
        	<div style="width: 25%;" role="progressbar" class="progress-bar progress-bar-warning progress-bar-striped active">
            	25%
        	</div>
        	<div style="width: 75%;" role="progressbar" class="progress-bar progress-bar-danger">
            	75%
        	</div>
    	</div>
	</div>
	<div class="col-xs-12 col-md-offset-2">
    	<div class="col-xs-8 col-md-offset-1">
        	<h3>
        	Contact Data
        	</h3>
    	</div>
	</div>
	
	
	<form class="form-horizontal" role="form" id="myform">
	<div class="col-xs-12">		
        		<div class="form-group">
            		<label class="control-label col-md-4 hidden-xs" for="name">Name</label>
            		<div class="col-md-2">
                		<input type="text" required="required" maxlength="50" placeholder="Name" id="name" class="form-control">
            		</div>
            	
            		<label class="control-label col-md-1 hidden-xs" for="surname">Surame</label>
            		<div class="col-md-2">
               			<input type="text" required="required" maxlength="50" placeholder="Surname" id="surname" class="form-control">
            		</div>
        		</div>
        		
        		<div class="form-group">
            		<label class="control-label col-md-4 hidden-xs" for="email">Email</label>
            			<div class="col-md-2">
                			<input type="email" required="required" maxlength="50" placeholder="Email" id="email" class="form-control">
            			</div>
            		<label class="control-label col-md-1 hidden-xs" for="password">Password</label>
            			<div class="col-md-2">
                			<input type="password" required="required" maxlength="10" placeholder="Password:" id="password" class="form-control">
            			</div>
        			</div>
        			<div class="form-group">
            			<div class="checkbox col-md-4 col-md-offset-4">
               			 <label>
                   				 <input type="checkbox">I agree with the terms and conditions
               			 </label>
            		</div>
        		</div>
        		<div class="form-group">
            		<div class="col-md-2 col-md-offset-4">
                		<button id="stepone" class="btn btn-primary">Next </button>
            		</div>
        		</div>
   
			</div>
	</form>
	</div>

<!-- End step One -->
    

<div id="centerMain" class="steptwo">
	<div class="fluid">
    <div class="progress">
        <div style="width: 25%;" role="progressbar" class="progress-bar progress-bar-success">
            25%
        </div>
        <div style="width: 25%;" role="progressbar" class="progress-bar progress-bar-warning progress-bar-striped active">
            25%
        </div>
        <div style="width: 50%;" role="progressbar" class="progress-bar progress-bar-danger">
            50%
        </div>
    </div>
	</div>
	<div class="col-xs-12 col-md-offset-2">
    <div class="col-xs-8 col-md-offset-1">
        <h3>Personal data</h3>
    </div>
	</div>
	<form class="form-horizontal" id="myformtwo">
	<div class="col-xs-12">
  
        <div class="form-group">
            <label class="control-label col-md-4 hidden-xs" for="phone">Phone</label>
            <div class="col-md-2">
                <input type="text" maxlength="50" placeholder="Phone:" id="phone" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-4 hidden-xs " for="birthday">Date of birth</label>
            <div class="col-md-2">
                <input type="text" placeholder="Date of birth:" id="birthday" class="form-control date">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-4 hidden-xs" for="gender">Gender</label>
            <div class="col-md-2">
                <select id="gender" class="form-control">
                    <option selected="" disabled="">Gender:</option>
                    <option>Male</option>
                    <option>Female</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-4 hidden-xs" for="province">Province</label>
            <div class="col-md-2">
                <input type="text" maxlength="50" placeholder="Province:" id="province" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-4 hidden-xs" for="zipcode">Zip code</label>
            <div class="col-md-2">
                <input type="text" maxlength="10" placeholder="Zip code:" id="zipcode" class="form-control">
            </div>
            <label class="control-label col-md-1 hidden-xs" for="city">City</label>
            <div class="col-md-2">
                <input type="text" maxlength="50" placeholder="City:" id="city" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-2 col-md-offset-4">
                <button id="steptwo" class="btn btn-primary">Next</button>
            </div>
        </div>
</div>
</form>
<!-- specified document js -->
</div>

<!-- End Step Two -->


<div id="centerMain" class="stepthree">
	<div class="fluid">
    	<div class="progress">
        	<div style="width: 50%;" role="progressbar" class="progress-bar progress-bar-success">
            	50%
        	</div>
        	<div style="width: 25%;" role="progressbar" class="progress-bar progress-bar-warning progress-bar-striped active">
            	25%
        	</div>
        	<div style="width: 25%;" role="progressbar" class="progress-bar progress-bar-danger">
            	25%
        	</div>
    	</div>
	</div>
	<div class="col-xs-12 col-md-offset-2">
    <div class="col-xs-8 col-md-offset-1">
        <h3>
            Size
        </h3>
    </div>
	</div>
	<form class="form-horizontal" role="form" id="myformthree" method="post" enctype="multipart/form-data">
	<div class="col-xs-12">
        <div class="form-group">
            <label class="control-label col-md-4 hidden-xs" for="height">Height</label>
            <div class="col-md-2">
                <input type="number" placeholder="Height [cm]:" id="height" class="form-control" step="1">
            </div>
            <label class="control-label col-md-1 hidden-xs" for="weight">Weight</label>
            <div class="col-md-2">
                <input type="number" placeholder="Weight [gr]:" id="weight" class="form-control" step="1">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-4 hidden-xs" for="eyeColor">Eye color</label>
            <div class="col-md-2">
                <select id="eyeColor" class="form-control">
                    <option selected="" disabled="">Eye color:</option>
                    <option>Black</option>
                    <option>Blue</option>
                    <option>Brown</option>
                    <option>Gray</option>
                    <option>Green</option>
                </select>
            </div>
            <label class="control-label col-md-1 hidden-xs" for="hairColor">Hair color</label>
            <div class="col-md-2">
                <select id="hairColor" class="form-control">
                    <option selected="" disabled="">Hair color:</option>
                    <option>Black</option>
                    <option>Brown</option>
                    <option>Blond</option>
                    <option>Red</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-4 hidden-xs" for="shirtSize">Shirt size</label>
            <div class="col-md-2">
                <input type="number" placeholder="Shirt size {cm]:" id="shirtSize" class="form-control" step="1">
            </div>
            <label class="control-label col-md-1 hidden-xs" for="neck">Neck</label>
            <div class="col-md-2">
                <input type="number" placeholder="Neck [cm]:" id="neck" class="form-control" step="1">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-4 hidden-xs" for="sleeveLength">Sleeve length</label>
            <div class="col-md-2">
                <input type="number" placeholder="Sleeve length [cm]:" id="sleeveLength" class="form-control" step="1">
            </div>
            <label class="control-label col-md-1 hidden-xs" for="jacket">Jacket</label>
            <div class="col-md-2">
                <input type="number" placeholder="Jacket [cm]:" id="jacket" class="form-control" step="1">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-4 hidden-xs" for="shoes">Shoes</label>
            <div class="col-md-2">
                <input type="number" placeholder="Shoes [EU]:" id="shoes" class="form-control" step="1">
            </div>
            <label class="control-label col-md-1 hidden-xs" for="trousers">Trousers</label>
            <div class="col-md-2">
                <input type="number" placeholder="Trousers [cm]:" id="trousers" class="form-control" step="1">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-4 hidden-xs" for="inseam">Inseam</label>
            <div class="col-md-2">
                <input type="number" placeholder="Inseam [cm]:" id="inseam" class="form-control" step="1">
            </div>
            <label class="control-label col-md-1 hidden-xs" for="dress">Dress</label>
            <div class="col-md-2">
                <input type="number" placeholder="Dress [cm]:" id="dress" class="form-control" step="1">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-4 hidden-xs" for="bust">Bust</label>
            <div class="col-md-2">
                <input type="number" placeholder="Bust [cm]:" id="bust" class="form-control" step="1">
            </div>
            <label class="control-label col-md-1 hidden-xs" for="band">Band</label>
            <div class="col-md-2">
                <input type="number" placeholder="Band [cm]:" id="band" class="form-control" step="1">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-4 hidden-xs" for="cup">Cup</label>
            <div class="col-md-2">
                <input type="number" placeholder="Cup [cm]:" id="cup" class="form-control" step="1">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-4 hidden-xs" for="profilePicture">Profile picture</label>
            <div class="col-md-2">
                <input type="file" id="profilePicture" class="form-control">
            </div>
            <label class="control-label col-md-1 hidden-xs" for="fullLengthPhoto">Photo</label>
            <div class="col-md-2">
                <input type="file" id="fullLengthPhoto" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-2 col-md-offset-4">
                <button id="stepthree" class="btn btn-primary">Next</button>
            </div>
        </div>
	</div>
	</form>
</div>


<!-- End Step Three -->
<div id="centerMain" class="stepfour">
	<div class="fluid">
    <div class="progress">
        <div style="width: 75%;" role="progressbar" class="progress-bar progress-bar-success">
            75%
        </div>
        <div style="width: 25%;" role="progressbar" class="progress-bar progress-bar-warning progress-bar-striped active">
            25%
        </div>
    </div>
	</div>
<div class="col-xs-12 col-md-offset-2">
    <div class="col-xs-8 col-md-offset-1">
        <h3>
            Shipping address
        </h3>
    </div>
</div>
<form class="form-horizontal" role="form" id="myformfour">
<div class="col-xs-12">
        <div class="form-group">
            <label class="control-label col-md-4 hidden-xs" for="street">Street</label>
            <div class="col-md-2">
                <input type="text" placeholder="Street:" id="street" class="form-control">
            </div>
            <label class="control-label col-md-1 hidden-xs" for="number">Number</label>
            <div class="col-md-2">
                <input type="text" placeholder="Number:" id="number" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-4 hidden-xs" for="floor">Floor</label>
            <div class="col-md-2">
                <input type="text" placeholder="Floor:" id="floor" class="form-control">
            </div>
            <label class="control-label col-md-1 hidden-xs" for="apartment">Apartment</label>
            <div class="col-md-2">
                <input type="text" placeholder="Apartment:" id="apartment" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-4 hidden-xs" for="province">Province</label>
            <div class="col-md-2">
                <input type="text" placeholder="Province:" id="province" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-4 hidden-xs" for="zipCode">Zip code</label>
            <div class="col-md-2">
                <input type="text" placeholder="Zip code:" id="zipCode" class="form-control">
            </div>
            <label class="control-label col-md-1 hidden-xs" for="city">City</label>
            <div class="col-md-2">
                <input type="text" placeholder="City:" id="city" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-4 hidden-xs" for="type">Type</label>
            <div class="col-md-2">
                <select id="type" class="form-control">
                    <option selected="" disabled="">Type:</option>
                    <option>Home</option>
                    <option>Office</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-2 col-md-offset-4">
                <button id="stepfour" class="btn btn-primary">Next</button>
            </div>
        </div>
</div>
</form>
</div>
<!-- End Step Four -->





<div id="workArea" class="finalstep"><hr class="style1">
    <div class="col-xs-12 col-md-offset-2">
        <div class="col-xs-8 col-md-offset-1">
            <h3>
                Contact Data
            </h3>
        </div>
    </div>
    <hr class="style1">
    
    <form class="form-horizontal" role="form" method="post" action="auth/sign_up_customer">
    <div class="form-group">
        <label class="control-label col-md-4 hidden-xs" for="name">Name</label>
        <div class="col-md-2">
            <input type="text" required="required" name="name" placeholder="Name:" id="names" class="form-control">
        </div>
        <label class="control-label col-md-1 hidden-xs" for="surname">Surame</label>
        <div class="col-md-2">
            <input type="text" required="required" name="surname" placeholder="Surname:" id="surnames" class="form-control">
        </div>
    </div>
    <input type="hidden" id="passwords" name="password" />
    <div class="form-group">
        <label class="control-label col-md-4 hidden-xs" for="email">Email</label>
        <div class="col-md-2">
            <input type="email" required="required" name="email" placeholder="Email:" id="emails" class="form-control">
        </div>
    </div>
    <br>
    
    
    
    <div class="col-xs-12 col-md-offset-2">
        <div class="col-xs-8 col-md-offset-1">
            <h3>
                Personal Data
            </h3>
        </div>
    </div>
    
    
    
    <div class="form-group">
        <label class="control-label col-md-4 hidden-xs" for="phone">Phone</label>
        <div class="col-md-2">
            <input type="text" maxlength="50" placeholder="Phone:" id="phones" name="phone" class="form-control">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-4 hidden-xs " for="birthday">Date of birth</label>
        <div class="col-md-2">
            <input type="text" placeholder="Date of birth:" id="birthdays" name="dob" class="form-control date">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-4 hidden-xs" for="gender">Gender</label>
        <div class="col-md-2">
            <select id="genders" name="gender" class="form-control">
                <option selected="" disabled="">Gender:</option>
                <option value="Male">Male</option>
                <option value="Female">Female</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-4 hidden-xs" for="province">Province</label>
        <div class="col-md-2">
            <input type="text" maxlength="50" placeholder="Province:" id="provinces" name="province" class="form-control">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-4 hidden-xs" for="zipcode">Zip code</label>
        <div class="col-md-2">
            <input type="text" maxlength="10" placeholder="Zip code:" name="zipcode" id="zipcodes" class="form-control">
        </div>
        <label class="control-label col-md-1 hidden-xs" for="city">City</label>
        <div class="col-md-2">
            <input type="text" maxlength="50" placeholder="City:" id="citys" name="city" class="form-control">
        </div>
    </div>
    <br>
    
    
    
    <div class="col-xs-12 col-md-offset-2">
        <div class="col-xs-8 col-md-offset-1">
            <h3>
                Size
            </h3>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-4 hidden-xs" for="height">Height</label>
        <div class="col-md-2">
            <input type="number" placeholder="Height [cm]:" id="heights" name="height" class="form-control" step="1">
        </div>
        <label class="control-label col-md-1 hidden-xs" for="weight">Weight</label>
        <div class="col-md-2">
            <input type="number" placeholder="Weight [gr]:" id="weights" name="weight" class="form-control" step="1">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-4 hidden-xs" for="eyeColor">Eye color</label>
        <div class="col-md-2">
            <select id="eyeColors" name="eyecolor" class="form-control">
                <option selected="" disabled="">Eye color:</option>
                <option value="Black">Black</option>
                <option value="Blue">Blue</option>
                <option value="Brown">Brown</option>
                <option value="Gray">Gray</option>
                <option value="Green">Green</option>
            </select>
        </div>
        <label class="control-label col-md-1 hidden-xs" for="hairColor">Hair color</label>
        <div class="col-md-2">
            <select id="hairColors" name="hairColor" class="form-control">
                <option selected="" disabled="">Hair color:</option>
                <option value="Black" >Black</option>
                <option value="Brown">Brown</option>
                <option value="Blond">Blond</option>
                <option value="Red">Red</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-4 hidden-xs" for="shirtSize">Shirt size</label>
        <div class="col-md-2">
            <input type="number" placeholder="Shirt size {cm]:" id="shirtSizes" name="shirtSize" class="form-control" step="1">
        </div>
        <label class="control-label col-md-1 hidden-xs" for="neck">Neck</label>
        <div class="col-md-2">
            <input type="number" placeholder="Neck [cm]:" id="necks" name="neck" class="form-control" step="1">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-4 hidden-xs" for="sleeveLength">Sleeve length</label>
        <div class="col-md-2">
            <input type="number" placeholder="Sleeve length [cm]:" id="sleeveLengths" name="sleeveLength" class="form-control" step="1">
        </div>
        <label class="control-label col-md-1 hidden-xs" for="jacket">Jacket</label>
        <div class="col-md-2">
            <input type="number" placeholder="Jacket [cm]:" id="jackets" name="jacket" class="form-control" step="1">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-4 hidden-xs" for="shoes">Shoes</label>
        <div class="col-md-2">
            <input type="number" placeholder="Shoes [EU]:" id="shoess" name="shoes" class="form-control" step="1">
        </div>
        <label class="control-label col-md-1 hidden-xs" for="trousers">Trousers</label>
        <div class="col-md-2">
            <input type="number" placeholder="Trousers [cm]:" id="trouserss" name="trousers" class="form-control" step="1">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-4 hidden-xs" for="inseam">Inseam</label>
        <div class="col-md-2">
            <input type="number" placeholder="Inseam [cm]:" id="inseams" name="inseam" class="form-control" step="1">
        </div>
        <label class="control-label col-md-1 hidden-xs" for="dress">Dress</label>
        <div class="col-md-2">
            <input type="number" placeholder="Dress [cm]:" id="dresss" name="dress" class="form-control" step="1">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-4 hidden-xs" for="bust">Bust</label>
        <div class="col-md-2">
            <input type="number" placeholder="Bust [cm]:" id="busts" name="bust" class="form-control" step="1">
        </div>
        <label class="control-label col-md-1 hidden-xs" for="band">Band</label>
        <div class="col-md-2">
            <input type="number" placeholder="Band [cm]:" id="bands" name="band" class="form-control" step="1">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-4 hidden-xs" for="cup">Cup</label>
        <div class="col-md-2">
            <input type="number" placeholder="Cup [cm]:" id="cups" name="cup" class="form-control" step="1">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-4 hidden-xs" for="profilePicture">Profile picture</label>
        <div class="col-md-2">
            <input type="file" id="profilePictures" name="profilePicture" class="form-control">
        </div>
        <label class="control-label col-md-1 hidden-xs" for="fullLengthPhoto">Photo</label>
        <div class="col-md-2">
            <input type="file" id="fullLengthPhotos" name="fullLengthPhoto" class="form-control">
        </div>
    </div>
    <br>
    <div class="col-xs-12 col-md-offset-2">
        <div class="col-xs-8 col-md-offset-1">
            <h3>
                Shipping address
            </h3>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-4 hidden-xs" for="deliveryStreet">Street</label>
        <div class="col-md-2">
            <input type="text" placeholder="street:" id="deliveryStreets" name="deliveryStreet" class="form-control">
        </div>
        <label class="control-label col-md-1 hidden-xs" for="deliveryNumber">Number</label>
        <div class="col-md-2">
            <input type="text" placeholder="Number:" id="deliveryNumbers" name="deliveryNumber" class="form-control">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-4 hidden-xs" for="deliveryFloor">Floor</label>
        <div class="col-md-2">
            <input type="text" placeholder="Floor:" id="deliveryFloors" name="deliveryFloor" class="form-control">
        </div>
        <label class="control-label col-md-1 hidden-xs" for="deliveryApartment">Apartment</label>
        <div class="col-md-2">
            <input type="text" placeholder="Apartment:" id="deliveryApartments" name="deliveryApartment" class="form-control">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-4 hidden-xs" for="deliveryProvince">Province</label>
        <div class="col-md-2">
            <input type="text" placeholder="Province:" id="deliveryProvinces" name="deliveryProvince" class="form-control">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-4 hidden-xs" for="deliveryZipCode">Zip code</label>
        <div class="col-md-2">
            <input type="text" placeholder="Zip code:" id="deliveryZipCodes" name="deliveryZipCode" class="form-control">
        </div>
        <label class="control-label col-md-1 hidden-xs" for="deliveryCity">City</label>
        <div class="col-md-2">
            <input type="text" placeholder="City:" id="deliveryCitys" name="deliveryCity" class="form-control">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-4 hidden-xs" for="deliveryType">Type</label>
        <div class="col-md-2">
            <select id="deliveryTypes" name="deliveryType" class="form-control">
                <option selected="" disabled="">Type:</option>
                <option value="Home">Home</option>
                <option value="Office">Office</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-2 col-md-offset-4">
           <input type="submit" name="submit" class="btn btn-primary" value="Save" /> 
        </div>
    </div>
    
   </form> 
</div>

<div class="clearfix"> </div>    
<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/additional-methods.min.js"></script>
<script>
// just for the demos, avoids form submit
jQuery.validator.setDefaults({
  debug: true,
  success: "valid"
});
var form = $( "#myform" );
form.validate();
$( "#stepone" ).click(function() {
  if(form.valid() == true )
  {
  	$(".stepone").hide();
    $(".steptwo").show(); 
    $("#names").val($('#name').val());
    $("#surnames").val($('#surname').val());
    $("#emails").val($('#email').val());
    $("#passwords").val($('#password').val());
  }
});

var formtwo = $( "#myformtwo" );
formtwo.validate();
$( "#steptwo" ).click(function() {
  if(formtwo.valid() == true )
  {
  			 $(".steptwo").hide();
        	 $(".stepthree").show();  
        	 $("#phones").val($('#phone').val());
        	 $("#birthdays").val($('#birthday').val());
        	 $("#genders").val($('#gender').val());
        	 $("#provinces").val($('#province').val());
        	 $("#zipcodes").val($('#zipcode').val());
        	 $("#citys").val($('#city').val());
  }
});
var formthree = $( "#myformthree" );
formthree.validate();
$( "#stepthree" ).click(function() {
  if(formthree.valid() == true )
  {
  			var imageone=$("#profilePicture").val();
  			var imagetwo=$("#fullLengthPhoto").val();
  			var data= {image: imageone, images: imagetwo}
					$.ajax({
						'url':'<?php echo base_url();?>auth/images',
						'type':'POST',
						'data':data,
						'success':function(message)
						{
							
									
									
						}
					});
  			$(".stepthree").hide();
        	$(".stepfour").show(); 
        	$("#heights").val($('#height').val());
        	$("#weights").val($('#weight').val());
        	$("#eyeColors").val($('#eyeColor').val());
        	$("#hairColors").val($('#hairColor').val());
        	$("#shirtSizes").val($('#shirtSize').val());
        	$("#necks").val($('#neck').val());
        	$("#sleeveLengths").val($('#sleeveLength').val());
        	$("#jackets").val($('#jacket').val());
        	$("#shoess").val($('#shoes').val());
        	$("#trouserss").val($('#trousers').val());
        	$("#inseams").val($('#inseam').val());
        	$("#dresss").val($('#dress').val());
        	$("#busts").val($('#bust').val());
        	$("#bands").val($('#band').val());
        	$("#cups").val($('#cup').val());
        	$("#profilePictures").val($('#profilePicture').val());
        	$("#fullLengthPhotos").val($('#fullLengthPhoto').val());
  }
});

var formfour = $( "#myformfour" );
formfour.validate();
$( "#stepfour" ).click(function() {
  if(formfour.valid() == true )
  {
  			$(".stepfour").hide();
        	$(".finalstep").show();  
        	$("#deliveryStreets").val($('#street').val());
        	$("#deliveryNumbers").val($('#number').val());
        	$("#deliveryFloors").val($('#floor').val());
        	$("#deliveryApartments").val($('#apartment').val());
        	$("#deliveryProvinces").val($('#province').val());
        	$("#deliveryZipCodes").val($('#zipCode').val());
        	$("#deliveryCitys").val($('#city').val());
        	$("#deliveryTypes").val($('#type').val());
  }
});

</script>
<script>
	$(document).ready(function(){
		var date_input=$('.date'); //our date input has the name "date"
		var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
		date_input.datepicker({
			format: 'dd-mm-yyyy',
			container: container,
			todayHighlight: true,
			autoclose: true, 
		})
	})
</script>
