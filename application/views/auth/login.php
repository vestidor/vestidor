<?php $this->layout('layouts::default') ?>
<div class="container">
	<hr class="style1">
	<div class="col-xs-12 col-md-offset-2">
		<div class="col-xs-8 col-md-offset-1">
			<h3> Login </h3>
		</div>
	</div>
	<div class="col-xs-10 col-md-offset-1">
	<?php 
			$ci=&get_instance();
			$ci->load->library('session');
			$a=$ci->session->flashdata('delete');
			if(!empty($a))  
			{
			?>
				<div class="alert alert-danger">
    				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    				<strong>Error !</strong> <?php print_r($ci->session->flashdata('delete')); ?>
  				</div>
			<?php 
			}
			?>
		<?php echo $form->open(); ?>

				<?php echo $form->messages(); ?>
	
				<div class="form-group col-md-12">
  					<div class="col-md-4 text-right">
  						<label for="email">Email</label>
  					</div>
  					<div class="col-md-4">
  						<input type="email" class="form-control" id="email" value="" name="email">
  					</div>
				</div>
				<div class="form-group col-md-12">
  						<div class="col-md-4 text-right">
  							<label for="password">Password</label>
  						</div>
 	 					<div class="col-md-4">
 	 						<input type="password" name="password" value="" id="password" class="form-control">
 	 					</div>
				</div>

				<div class="col-md-8 col-md-offset-4">
					<label>
							<input type="checkbox" name="remember"> Remember me
					</label>
				</div>
					 
				<div class="col-md-8 col-md-offset-4">
					<a href="auth/forgot_password">Forgot password?</a>
				</div>
				<div class="col-md-8 col-md-offset-4 ">
					<?php echo $form->bs3_submit('Login'); ?>
					<a href="register" class="btn btn-danger">Register</a>
				</div>	
				<?php echo $form->close(); ?>
</div>
</div>