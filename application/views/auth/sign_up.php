<?php $this->layout('layouts::default') ?>

<?php // append scripts to <head> ?>
<?php $this->start('scripts_head') ?>
<?php $this->stop() ?>
<div class="container">
<?php echo $form->open(); ?>
	
	<?php echo $form->messages(); ?>

	<?php echo $form->bs3_text('Name', 'username'); ?>
	<?php echo $form->bs3_email('Email'); ?>
	<?php echo $form->bs3_password('Password', 'password'); ?>
	<?php echo $form->bs3_password('Retype Password', 'retype_password'); ?>
	<div class="form-group">
		Have an Account? <a href="login">Log In</a>
	</div>
	
	<?php echo $form->bs3_submit('Sign Up'); ?>

<?php echo $form->close(); ?>
</div>