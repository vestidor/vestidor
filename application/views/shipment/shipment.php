<?php $this->layout('layouts::default') ?>
<?php
$ci=&get_instance();
$ci->load->helper('common_helper');
?>
<?php 
	$ci =& get_instance();
	$ci->load->library('session');
	$x=$ci->session->userdata('user_group');
	$y=$ci->session->userdata();
	$users= $y['user_id'];
?>
<div class="row">
	<div class="col-md-12">
		<h1>Shipment</h1>
		<hr />
	</div>
	<div class="col-md-10 text-right">
			<a href="account" class="btn btn-primary">Account</a>
			<a href="logout" class="btn btn-danger">LogOut</a>
		</div>
		<?php if($x[0]->group_id == 4){ ?>		
		<div class="col-md-12">
			<div class="col-md-2">&nbsp;</div>
			<div class="col-md-8" style="border-bottom: 1px solid;">
				<table>
					<tr>
						<td style="padding: 5px;">Created Date</td>
						<td style="padding: 5px;">Order Date</td>
						<td style="padding: 5px;">Withdraw Date</td>
						<td style="padding: 5px;">Status</td>
					</tr>
					<?php foreach ($result as $re) { ?>
					<tr>
						<td style="padding: 5px;"><?php echo date('d-m-Y' ,strtotime($re->createdOn)); ?></td>
						<td style="padding: 5px;"><?php if($re->orderDate != '0000-00-00') { echo date('d-m-Y' ,strtotime($re->orderDate)); } else { echo "--"; } ?></td>
						<td style="padding: 5px;"><?php if($re->withdrawDate != '0000-00-00') { echo date('d-m-Y' ,strtotime($re->withdrawDate)); } else { echo "--"; } ?></td>
						<td style="padding: 5px;">
							<?php if($re->status == 1) { ?>
								in process
							<?php } elseif ($re->status == 2) { ?>
								on delivery
							<?php } else { ?>
									Completed
							<?php } ?>		
						</td>
					</tr>
					<?php } ?>	
				</table>
			</div>
		</div>
		<?php } ?>
		<?php if($x[0]->group_id == 3){ ?>
			
			<div class="col-md-12">
			<div class="col-md-2">&nbsp;</div>
			<div class="col-md-8" style="border-bottom: 1px solid;">
				<table>
					<tr>
						<td style="padding: 5px;">Customer</td>
						<td style="padding: 5px;">Created Date</td>
						<td style="padding: 5px;">Order Date</td>
						<td style="padding: 5px;">Withdraw Date</td>
						<td style="padding: 5px;">Status</td>
					</tr>
					<?php foreach ($stylistdata as $s) { ?>
					<tr>
						<td><?php $ttt = get_customername($s->userID); ?>
							<?php foreach($ttt as $t) { ?>
								<?php echo $t->username; ?>
							<?php } ?>	
						</td>
						<td style="padding: 5px;"><?php echo date('d-m-Y' ,strtotime($s->createdOn)); ?></td>
						<td style="padding: 5px;"><?php if($s->orderDate != '0000-00-00') { echo date('d-m-Y' ,strtotime($s->orderDate)); } else { echo "--"; } ?></td>
						<td style="padding: 5px;"><?php if($s->withdrawDate != '0000-00-00') { echo date('d-m-Y' ,strtotime($s->withdrawDate)); } else { echo "--"; } ?></td>
						<td style="padding: 5px;">
							<?php if($s->status == 1) { ?>
								in process
							<?php } elseif ($s->status == 2) { ?>
								on delivery
							<?php } else { ?>
									Completed
							<?php } ?>		
						</td>
					</tr>
					<?php } ?>	
				</table>
			</div>
		</div>
			
		<?php } ?>
</div>