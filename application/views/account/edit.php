<?php $this->layout('layouts::default') ?>
<?php 
	$ci =& get_instance();
	$ci->load->library('session');
	$x=$ci->session->userdata('user_group');
	$y=$ci->session->userdata();
	$users= $y['user_id'];
?>

<div class="row">
	<div class="col-md-12">
		<h1>Profile</h1>
	</div>
	<?php if($x[0]->group_id == 4){ ?>
	<div class="col-md-10 text-right">
		<a href="account" class="btn btn-primary">Account</a>
		<a href="logout" class="btn btn-danger">LogOut</a>
	</div>		
	<form method="post" enctype="multipart/form-data" action="account/update">	
	<div class="col-md-8">
	<?php foreach($result as $res){ ?>
	<input type="hidden" name="id" value="<?php echo $res->ID; ?>" />	
	<div class="col-md-12 form-group">
			<div class="col-md-4">Phone Number One:-</div>
			<div class="col-md-6"><input type="text" class="form-control" name="phoneOne" value="<?php echo $res->phoneOne; ?>"/></div>
		</div>
		<div class="col-md-12 form-group">
			<div class="col-md-4">Phone Number Two:-</div>
			<div class="col-md-6"><input type="text" class="form-control" name="phoneTwo" value="<?php echo $res->phoneTwo; ?>" /></div>
		</div>
		<div class="col-md-12 form-group">
			<div class="col-md-4">Office Address:-</div>
			<div class="col-md-6"><input type="text" class="form-control" name="officeAddress" value="<?php echo $res->officeAddress; ?>" /></div>
		</div>
		<div class="col-md-12 form-group">
			<div class="col-md-4">Home Address:-</div>
			<div class="col-md-6"><input type="text" class="form-control" name="homeAddress" value="<?php echo $res->homeAddress; ?>" /></div>
		</div>
		<div class="col-md-12 form-group">
			<div class="col-md-4">Office Zip Code:-</div>
			<div class="col-md-6"><input type="text" class="form-control" name="officeZip" value="<?php echo $res->officeZip; ?>" /></div>
		</div>
		<div class="col-md-12 form-group">
			<div class="col-md-4">Home Zip Code:-</div>
			<div class="col-md-6"><input type="text" class="form-control" name="homeZip" value="<?php echo $res->homeZip; ?>" /></div>
		</div>
		<div class="col-md-12 form-group">
			<div class="col-md-4">Gender:-</div>
			<div class="col-md-6">
				<select name="gender" class="form-control">
					<option value="">Select</option>
					<option value="M" <?php if($res->gender == 'M') { echo "selected='selected'"; } ?>>Male</option>
					<option value="F" <?php if($res->gender == 'F') { echo "selected='selected'"; } ?>>Female</option>
				</select>
			</div>
		</div>
		<div class="col-md-12 form-group">
			<div class="col-md-4">Birth Date:-</div>
			<div class="col-md-6"><input type="text" class="form-control date" id="datepicker" name="birthDate" value="<?php echo date ("d-m-Y",strtotime($res->birthDate));?>" /></div>
		</div>
		<div class="col-md-12 form-group">
			<div class="col-md-4">Body Height:-</div>
			<div class="col-md-6"><input type="text" class="form-control" name="bodyHeight" value="<?php echo $res->bodyHeight; ?>" /></div>
		</div>
		<div class="col-md-12 form-group">
			<div class="col-md-4">Body Weight:-</div>
			<div class="col-md-6"><input type="text" class="form-control" name="bodyWeight" value="<?php echo $res->bodyWeight; ?>" /></div>
		</div>
		
		<div class="col-md-12 form-group">
			<div class="col-md-4">Shirt Size:-</div>
			<div class="col-md-6"><input type="text" class="form-control" name="sizeShirt" value="<?php echo $res->sizeShirt; ?>" /></div>
		</div>
		
		<div class="col-md-12 form-group">
			<div class="col-md-4">Jeans Width:-</div>
			<div class="col-md-6"><input type="text" class="form-control" name="jeansWidth" value="<?php echo $res->jeansWidth; ?>" /></div>
		</div>
		
		<div class="col-md-12 form-group">
			<div class="col-md-4">Jeans Height:-</div>
			<div class="col-md-6"><input type="text" class="form-control" name="jeansHeight" value="<?php echo $res->jeansHeight; ?>" /></div>
		</div>
		
		<div class="col-md-12 form-group">
			<div class="col-md-4">Shoe Size:-</div>
			<div class="col-md-6"><input type="text" class="form-control" name="shoeSize" value="<?php echo $res->shoeSize; ?>" /></div>
		</div>
		
		<div class="col-md-12 form-group">
			<div class="col-md-4">Withdrawal Address:-</div>
			<div class="col-md-6"><input type="text" class="form-control" name="withdrawalAddress" value="<?php echo $res->withdrawalAddress; ?>" /></div>
		</div>
		<?php } ?>
			
		<div class="col-md-12 form-group">
			<div class="col-md-6"><input type="submit" name="submit" value="Edit Profile" class="btn btn-success" /></div>
		</div>
		</div>
		</form>
		<?php } ?>
		<?php if($x[0]->group_id == 3){ ?>	
		<div class="col-md-10 text-right">
			<a href="account" class="btn btn-primary">Account</a>
			<a href="logout" class="btn btn-danger">LogOut</a>
		</div>	
			
		<form method="post" enctype="multipart/form-data" action="account/update_stylist">
			<div class="col-md-8">
				<?php foreach($stylist as $s){ ?>
					<input type="hidden" name="id" value="<?php echo $s->id; ?>" />	
					<div class="col-md-12 form-group">
						<div class="col-md-4">Name:-</div>
						<div class="col-md-6"><input type="text" class="form-control" name="username" value="<?php echo $s->username; ?>"/></div>
					</div>
					
					<div class="col-md-12 form-group">
						<div class="col-md-4">email:-</div>
						<div class="col-md-6"><input type="text" class="form-control" name="email" value="<?php echo $s->email; ?>"/></div>
					</div>
					
					<div class="col-md-12 form-group">
						<div class="col-md-4">Phone Number One:-</div>
						<div class="col-md-6"><input type="text" class="form-control" name="phoneOne" value="<?php echo $s->phoneOne; ?>"/></div>
					</div>
					
					<div class="col-md-12 form-group">
						<div class="col-md-4">Phone Number Two:-</div>
						<div class="col-md-6"><input type="text" class="form-control" name="phoneTwo" value="<?php echo $s->phoneTwo; ?>"/></div>
					</div>
					
					<div class="col-md-12 form-group">
						<div class="col-md-4">Address:-</div>
						<div class="col-md-6"><input type="text" class="form-control" name="address" value="<?php echo $s->address; ?>"/></div>
					</div>
				<?php } ?>
					<div class="col-md-12 form-group">
						<div class="col-md-6"><input type="submit" name="submit" value="Edit Profile" class="btn btn-success" /></div>
					</div>
				</div>	
		</form>	
		<?php } ?>
</div>
<script>
	$(document).ready(function(){
		var date_input=$('.date'); //our date input has the name "date"
		var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
		date_input.datepicker({
			format: 'dd-mm-yyyy',
			container: container,
			todayHighlight: true,
			autoclose: true,
		})
	})
</script>