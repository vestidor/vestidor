<?php $this->layout('layouts::default') ?>
<?php 
	$ci =& get_instance();
	$ci->load->library('session');
	$x=$ci->session->userdata('user_group');
	$y=$ci->session->userdata();
	$users= $y['user_id'];
?>

<div class="row">
	<div class="col-md-12">
		<h1>Account</h1>
	</div>
	<div class="col-md-6">
		<?php $ci=&get_instance(); 
		$ci->load->library('session');
		$job_post=$ci->session->flashdata('job_post_success');
		if(!empty($job_post))
		{
		?>
		<div class="alert alert-success">
    		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    		<strong>Success!</strong> <?php print_r($job_post); ?>
  		</div>
		<?php } ?>
	</div>
	
	<div class="col-md-6">
		<?php $ci=&get_instance(); 
		$ci->load->library('session');
		$job_post=$ci->session->flashdata('job_post');
		if(!empty($job_post))
		{
		?>
		<div class="alert alert-danger">
    		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    		<strong>Error!</strong> <?php print_r($job_post); ?>
  		</div>
		<?php } ?>
	</div>
	<?php if($x[0]->group_id == 4){ ?>	
	<?php foreach($result as $res){ ?>	
	<div class="col-md-10 text-right">
		<a href="stylist/<?php echo $res->stylistID; ?>" class="btn btn-success">My Stylist</a>
		<a href="order" class="btn btn-info">My Order</a>
		<a href="shipment" class="btn btn-warning">My Shipment</a>
		<a href="account/edit/<?php echo $users; ?>" class="btn btn-primary">Profile Edit</a>
		<a href="box/<?php echo $res->stylistID; ?>" class="btn btn-info">Box Order</a>
		<a href="auth/delete" class="btn btn-danger" onclick="return confirm('Are you sure?')">Delete Account</a>
		<a href="logout" class="btn btn-danger">LogOut</a>
	</div>
	<div class="col-md-12">
	
					
	</div>
	<?php } ?>	
	<?php } ?>
	<?php if($x[0]->group_id == 3){ ?>
		<div class="col-md-12">
			<?php foreach($stylist as $s) { ?>
				<h1>Wel Come <?php echo $s->username; ?></h1>
				<hr />
				<div class="col-md-10 text-right">
					<a href="shipment" class="btn btn-warning">Shipment</a>
					<a href="customers/<?php echo $s->ID; ?>" class="btn btn-success">My Customer</a>
					<a href="account/edit/<?php echo $users; ?>" class="btn btn-primary">Profile Edit</a>
					<a href="logout" class="btn btn-danger">LogOut</a>
				</div>
				<div class="col-md-12">
					<div class="col-md-6">
						<div class="col-md-6">Name:</div><div class="col-md-6"><?php echo $s->username; ?></div>
						<div class="col-md-6">Email:</div><div class="col-md-6"><?php echo $s->email; ?></div>
						<div class="col-md-6">Phone Numner One:</div><div class="col-md-6"><?php echo $s->phoneOne; ?></div>
						<div class="col-md-6">Phone Number Two:</div><div class="col-md-6"><?php echo $s->phoneTwo; ?></div>
						<div class="col-md-6">Address</div><div class="col-md-6"><?php echo $s->address; ?></div> 
					</div>
				</div>
			<?php } ?>
		</div>
	<?php } ?>
</div>