<div id="box-mobile-menu" class="box-mobile-menu full-height full-width">
	<div class="box-inner">
		<span class="close-menu"><span class="icon pe-7s-close" style="color:#fff;"></span></span>
	</div>
</div>

<!-- Header on top -->
<div id="header-ontop" class="is-sticky"></div>
<!-- /Header on top -->
<!-- Header below -->

<div id="header-below">
	<header id="header" class="header style3 style15">
		<div class="container">
			<div class="main-menu-wapper">
				<div class="row">
					<div class="col-sm-12 col-md-2 col-lg-3 logo-wapper hidden-xs">
						<div class="logo">
							<a href="<?php echo base_url(); ?>"><img src="<?php echo dist_url('vertidor/images/logos/1.png'); ?>" alt="logo"></a>
						</div>
					</div>
					<div class="col-sm-12 col-md-10 col-lg-9 menu-wapper">
						<div class="section-title text-center mt0 mb0 hidden-xs" style="border-bottom:1px solid #cecece; padding:20px;">
							<h4 class="mb0"><span id="gmail-docs-internal-guid-bb814847-e4ff-663f-5052-05309df43165"> SI EN LA TIENDA NO PAGAS POR PROBARTE LA ROPA ¿POR QUÉ HACERLO CUANDO COMPRAS ONLINE? </span></h4>
						</div>

						<div class="box-control">
							<div class="box-settings">
								<a href="login" class="icon pe-7s-user"></a>
							</div>
							<div id="logOffIndex" class="box-settings" style="display:none;">
								<a class="icon pe-7s-delete-user"></a>
							</div>
							<div class="box-settings">
								<span class="icon pe-7s-config"></span>
								<div class="settings-wrapper ">
									<div class="setting-content">
										<div class="select-language">
											<div class="language-title">
												Seleccionar idioma
											</div>
											<div class="language-topbar">
												<div class="lang-list">
													<ul class="clearfix">
														<li class="active">
															<a href="#"> <img src="<?php echo dist_url('vertidor/images/flag1.png'); ?>" alt=""> </a>
														</li>
														<li>
															<a href="#"> <img src="<?php echo dist_url('vertidor/images/flag2.png'); ?>" alt=""></a>
														</li>
														<li>
															<a href="#"> <img src="<?php echo dist_url('vertidor/images/flag3.png'); ?>" alt=""></a>
														</li>
													</ul>
												</div>
											</div>
										</div>

									</div>
								</div>
							</div>
						</div>

						<ul class="boutique-nav main-menu clone-main-menu">
							<li style="padding:0;">
								<img src="<?php echo dist_url('vertidor/images/logos/logo4.png'); ?>" alt="DESNUDIO" class="logo-ontop" style="margin-bottom:-5px;">
							</li>
							<li class="active">
								<a onclick="selected(this)" class="page-scroll" href="<?php echo base_url(); ?>">Home</a>
							</li>
							<li>
								<a onclick="selected(this)" class="page-scroll" href="<?php echo base_url(); ?>#about" />¿qué es DESNUDIO?</a>
							</li>
							<li>
								<a onclick="selected(this)" class="page-scroll" href="<?php echo base_url(); ?>#how">¿CÓMO FUNCIONA?</a>
							</li>
							<!-- productos oculto
							<li> <a onclick="selected(this)" class="page-scroll" href="#prod">PRODUCTOS</a> </li>
							-->

							<li>
								<a onclick="selected(this)" class="page-scroll" href="<?php echo base_url(); ?>#suscribite">SUSCRÍBETE</a>
							</li>
							<li>
								<a  href="blog">BLOG</a>
							</li>
						</ul>
						<span class="mobile-navigation"><i class="fa fa-bars"></i></span>
						<div class="text-center visible-xs" style="float:right; width:100%; position:absolute; top:5px; z-index:-1; padding:0 20%;"><img src="<?php echo dist_url('vertidor/images/logos/logo4.png'); ?>" alt="DESNUDIO" style="height:20px;">
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
</div>
<!-- /Header below -->
