<?php $this->layout('layouts::default') ?>
<?php 
	$ci =& get_instance();
	$ci->load->library('session');
	$x=$ci->session->userdata('user_group');
	$y=$ci->session->userdata();
	$users= $y['user_id'];
?>
<div class="row">
	<div class="col-md-12">
		<h1>My Customer</h1>
		<hr />
	</div>
	<div class="col-md-12">
		<div class="col-md-10 text-right">
			<a href="account" class="btn btn-primary">Account</a>
			<a href="logout" class="btn btn-danger">LogOut</a>
		</div>	
		<div class="col-md-8" style="border-bottom: 1px solid;">
			<table>
				<tr>
					<td style="padding: 5px;">Name</td>
					<td style="padding: 5px;">Email</td>
					<td style="padding: 5px;">Phone Number</td>
					<td style="padding: 5px;">Requested Date</td>
					<td style="padding: 5px;">status</td>
				</tr>
				<?php foreach($result as $res) { ?>
				<tr>
						<td style="padding: 5px;"><?php echo $res->username; ?></td>
						<td style="padding: 5px;"><?php echo $res->email; ?></td>
						<td style="padding: 5px;"><?php echo $res->phoneOne; ?> - <?php echo $res->phoneTwo; ?></td>
						<td style="padding: 5px;"><?php echo date('d-m-Y' ,strtotime($res->createdOn)); ?></td>
						<td style="padding: 5px;">
						<?php if($res->boxStatus == '0') { ?>
							<a href="products/<?php echo $res->ID; ?>">Assemble Products</a>
						<?php } else { ?>
								Completed
						<?php } ?>	
						</td>
				</tr>
				<?php } ?>
			</table>
		</div>
	</div>

</div>