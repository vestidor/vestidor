<?php $this->layout('layouts::default') ?>
<!-- Home slide -->
<div class="home-slide3 owl-carousel nav-style4 nav-center-center " data-items="1" data-nav="true" data-dots="false" data-loop="true" data-autoplay="true">	            	
			<a href="#"><img src="<?php echo dist_url('vertidor/images/slides/slide001.jpg'); ?>" alt=""> </a>
			<a href="#"><img src="<?php echo dist_url('vertidor/images/slides/slide002.jpg'); ?>" alt=""> </a>
			<a href="#"><img src="<?php echo dist_url('vertidor/images/slides/slide003.jpg'); ?>" alt=""> </a>
		
		</div>
		<!-- ./Home slide -->
				
		<div id="mainContainer">
			

			<div class="bg-03" style="position: relative;">
				<div class="aidis-about" id="about"> about</div>
				<div class="container">
					<div class="our-category pt72 pb72" style="border: 0px;">
						<div class="row">
							<div class="col-sm-12 col-md-12 col-lg-12">
								<div class="section-title text-center margin-top-40 margin-bottom-30">
									<h1 class="text-white text-uppercase wow fadeIn">Un estilista personal en tu casa</h1>
									<h4 class="mt16 text-white wow fadeIn" style="animation-delay:0.5s;">VESTIRSE CON DESNUDIO ES FÁCIL:</h4>
								</div>
							</div>
							<div class="col-sm-12 col-md-12 col-lg-12">
								<!-- row pasos -->
								<div class="row">
									<div class="col-xs-6 col-sm-3 text-center wow slideInRight">
										<img src="<?php echo dist_url('vertidor/images/paso1-gold.png'); ?>" alt="image" class="mb16">
										<h6 class="text-white">D&eacute;janos conocer tu estilo.</h6>
									</div>

									<div class="col-xs-6 col-sm-3 text-center wow slideInRight" style="animation-delay:0.5s;">
										<img src="<?php echo dist_url('vertidor/images/paso2-gold.png'); ?>" alt="image" class="mb16">
										<h6 class="text-white">Recibe una selecci&oacute;n exclusiva de prendas.</h6>
									</div>

									<div class="col-xs-6 col-sm-3 text-center wow slideInRight" style="animation-delay:1s;">
										<img src="<?php echo dist_url('vertidor/images/paso3-gold.png'); ?>" alt="image" class="mb16">
										<h6 class="text-white">Pru&eacute;bate los productos c&oacute;modamente en tu casa.</h6>
									</div>

									<div class="col-xs-6 col-sm-3 text-center wow slideInRight" style="animation-delay:1.5s;">
										<img src="<?php echo dist_url('vertidor/images/paso4-gold.png'); ?>" alt="image" class="mb16">
										<h6 class="text-white">Decide qu&eacute; prendas te gustan y devuelve las que no te convenzan.</h6>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="bg-03" style="position:relative;">
					<div class="aidis-how" id="how"></div>
				</div>

			</div>
			<!-- -./container pasos -->

			<div class="clearfix"> </div>

			<!-- como funca-->
			<section class="pt72 pb56">
				<div class="container">
					<div class="section-content text-center wow slideInDown">
						<h1 class="title">¿CÓMO FUNCIONA DESNUDIO?</h1>
						<h3 class=" text-info">COMPRAR EN DESNUDIO ES MUY F&Aacute;CIL Y C&Oacute;MODO</h3>
					</div>
				</div>
			</section>

			<div style="position:relative;">
				<!-- Block banner -->
				<div class=" mt16">
					<div class="container">
						<div class="row">
							<div class="col-sm-7">
								<div class="butique-banner wow fadeIn">
									<div class="image">
										<img src="<?php echo dist_url('vertidor/images/b/29.jpg'); ?>" alt="">
									</div>
									<div class="banner-inner wow slideInLeft" style="animation-delay:0.5s;">
										<div class="top">
											<div class="head" style="margin-left:0px;">
												<h4 class="text-uppercase">D&eacute;janos conocer tu estilo</h4>
												</div>
										</div>
										<div class="bottom">
											<div class="desc">
												<p>A trav&eacute;s de unas breves preguntas sobre lo que mas te gusta.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-5">
								<div class="butique-banner style2 wow fadeIn" style="animation-delay:3s;">
									<div class="image">
										<img src="<?php echo dist_url('vertidor/images/b/30.jpg'); ?>" alt="">
									</div>
									<div class="banner-inner wow slideInRight" style="animation-delay:3.5s;">
										<div class="top">
											<div class="head">
												<h4 class="text-uppercase">Recibe una selecci&oacute;n exclusiva de prendas</h4>
												</div>
										</div>
										<div class="bottom">
											<div class="desc">
												<p>De tu talla y tu estilo que nuestras estilistas eligen para t&iacute;.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- ./Block banner -->

				<!-- Block banner -->
				<div class="margin-top-80">
					<div class="container">
						<div class="row">
							<div class="col-sm-5">
								<div class="butique-banner style2 wow fadeIn" style="animation-delay:6s;">
									<div class="image">
										<img src="<?php echo dist_url('vertidor/images/b/31.jpg'); ?>" alt="">
									</div>
									<div class="banner-inner wow slideInLeft" style="animation-delay:6.5s;">
										<div class="top">
											<div class="head">
												<h4 class=" text-uppercase">Pru&eacute;bate los productos c&oacute;modamente en tu casa</h4>
											</div>
										</div>

										<div class="bottom">
											<div class="desc">
												<p>Sin esperas, sin agobios y sin cargar con bolsas.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-7">
								<div class="butique-banner wow fadeIn" style="animation-delay:8s;">
									<div class="image">
										<img src="<?php echo dist_url('vertidor/images/b/32.jpg'); ?>" alt="">
									</div>
									<div class="banner-inner wow slideInRight" style="animation-delay:8.5s;">
										<div class="top">
											<div class="head" style="margin-left:0px;">
												<h4 class=" text-uppercase">Decide qu&eacute; prendas te gustan y devuelve las que no te convenzan.</h4>
											</div>
										</div>
										<div class="bottom">
											<div class="desc">
												<p>Sin gastos de env&iacute;o y pagando &uacute;nicamente por lo que eliges.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- ./Block banner -->

				<div class="aidis-prod" id="prod"></div>

			</div>
			<!-- ./Como funca -->


			<!-- Productos -->
			<div class="block-paralax3 mt0 mb0">
				<div class="container">
					<div class="section-content text-center wow fadeIn">
						<h2 class="title"><strong> ¿QUÉ PRODUCTOS VAMOS A OFRECERTE?</strong></h2>
						<h3 class="sub-title text-primary">EN TU CAJA ENCONTRARAS PROPUESTAS DE</h3>
					</div>
				</div>
			</div>


			<!-- Group banner-->
			<div class="mt0">
				<div class="group-banner-masonry">
					<div class="grid-sizer"></div>
					<div class="banner-masonry-item">
						<div class="inner">
							<img src="<?php echo dist_url('vertidor/images/b/15-20.png'); ?>" alt="">
							<div class="content">
								<h3 class="subtitle">autumn fashion</h3>
							</div>
						</div>
					</div>
					<div class="banner-masonry-item wow fadeIn" style="animation-delay:0.5s;">
						<div class="inner">
							<img src="<?php echo dist_url('vertidor/images/b/15-21.png'); ?>" alt="">
							<div class="content">
								<h3 class="subtitle">autumn fashion</h3>
							</div>
						</div>
					</div>
					<div class="banner-masonry-item wow fadeIn" style="animation-delay:1s;">
						<div class="inner">
							<img src="<?php echo dist_url('vertidor/images/b/15-22.png'); ?>" alt="">
							<div class="content">
								<h3 class="subtitle">autumn fashion</h3>
							</div>
						</div>
					</div>
					<div class="banner-masonry-item wow fadeIn" style="animation-delay:3s;">
						<div class="inner">
							<img src="<?php echo dist_url('vertidor/images/b/15-23.png'); ?>" alt="">
							<div class="content">
								<h3 class="subtitle">autumn fashion</h3>
							</div>
						</div>
					</div>
					<div class="banner-masonry-item banner-masonry-item-2x wow fadeIn" style="animation-delay:2s;">
						<div class="inner">
							<img src="<?php echo dist_url('vertidor/images/b/15-24.png'); ?>" alt="">
							<div class="content">
								<h3 class="subtitle">autumn fashion</h3>
							</div>
						</div>
					</div>
					<div class="banner-masonry-item banner-masonry-item-2x wow fadeIn" style="animation-delay:3s;">
						<div class="inner">
							<img src="<?php echo dist_url('vertidor/images/b/15-25.png'); ?>" alt="">
							<div class="content">
								<h3 class="subtitle">autumn fashion</h3>
							</div>
						</div>
					</div>
					<div class="banner-masonry-item wow fadeIn" style="animation-delay:1.5s;">
						<div class="inner">
							<img src="<?php echo dist_url('vertidor/images/b/15-26.png'); ?>" alt="">
							<div class="content">
								<h3 class="subtitle">autumn fashion</h3>
							</div>
						</div>
					</div>
					<div class="banner-masonry-item wow fadeIn" style="animation-delay:3s;">
						<div class="inner">
							<img src="<?php echo dist_url('vertidor/images/b/15-27.png'); ?>" alt="">
							<div class="content">
								<h3 class="subtitle">autumn fashion</h3>
							</div>
						</div>
					</div>
					<div class="banner-masonry-item wow fadeIn" style="animation-delay:0.5s;">
						<div class="inner">
							<img src="<?php echo dist_url('vertidor/images/b/15-28.png'); ?>" alt="">
							<div class="content">
								<h3 class="subtitle">autumn fashion</h3>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- ./Group banner-->
			<!-- ./Productos -->


			<!-- suscribite -->
			<div style="position:relative;">
				<div class="aidis-suscribe" id="suscribite"></div>
			</div>

			<div class="container">
				<div class=" mt64 mb64">
					<div class="section-title text-center mt64 mb48 wow fadeIn">
						<h1>SUSCRÍBETE A DESNUDIO</h1>
						<h4><i style="font-family: 'Playfair Display' , 'Times New Roman', Times, serif;">y recibe tu primera caja en tres d&iacute;as.</i> </h4>
					</div>
					<div class="row">
						<div class="col-sm-12 col-md-7 wow flipInX">
							<div class="video video-lightbox">
								<img src="<?php echo dist_url('vertidor/images/img-newsletter.jpg'); ?>" alt="">
							</div>
						</div>
						<div class="col-sm-12 col-md-5 wow flipInX">
							<div class="newsletter">
								<div class="section-title text-left"><h3>NEWSLETTER</h3></div>
								<i class="newsletter-info text-left">Envianos tu e-mail y recib&iacute; semanalmente novedades de producto y ofertas.</i>
								<form class="form-newsletter">
									<input type="text" name="newsletter" placeholder="Tu e-mail.." value="">
									<span><button class="newsletter-submit" type="submit">enviar</button></span>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- ./suscribite -->
		
			<!-- Customer site -->
		<script type="text/javascript" src="<?php echo dist_url('vertidor/js/index.js'); ?>"></script>
		<script type="text/javascript" src="<?php echo dist_url('vertidor/js/jquery.cookie.js'); ?>"></script>

		<!-- de foundry -->
		<script src="<?php echo dist_url('vertidor/js/jquery.min.js'); ?>"></script>
		<script src="<?php echo dist_url('vertidor/js/bootstrap.min.js'); ?>"></script>
		<script src="<?php echo dist_url('vertidor/js/flexslider.min.js'); ?>"></script>
		<script src="<?php echo dist_url('vertidor/js/js/lightbox.min.js'); ?>"></script>
		<script src="<?php echo dist_url('vertidor/js/smooth-scroll.min.js'); ?>"></script>
		<script src="<?php echo dist_url('vertidor/js/parallax.js'); ?>"></script>
		<script src="<?php echo dist_url('vertidor/js/scripts.js'); ?>"></script>
		<script src="<?php echo dist_url('vertidor/js/jquery.nav.js'); ?>"></script>
		<script src="<?php echo dist_url('vertidor/js/wow.js'); ?>"></script>

		<script>

			// --------------------------------------------
			// Script to handle menu selection after click
			// --------------------------------------------

			function selected(e) {

				var $this = $(e).parent();
				var $parent = $(e).parent().parent().parent().parent().parent().parent().parent().parent().attr('id')
				// Ignore if already active
				if ($parent == "header-ontop" || $parent == "header-below") {
					if (!$this.hasClass('active')) {
						// Remove previously active item
						$('.main-menu li').removeClass('active');
						// Select the current one as active.
						$($this).addClass('active');
					}
					if ($parent == "header-below") {
						var content = $( "#header" ).clone(true, true);
						content.attr('id', 'headerTop');
						$("#header-ontop").empty();
						content.appendTo( "#header-ontop" );
					}
					if ($parent == "header-ontop") {
						var content = $( "#headerTop" ).clone(true, true);
						content.attr('id', 'header');
						content.removeClass('ontop');
						$("#header-below").empty();
						content.appendTo( "#header-below" );
					}
				}
			};

			// --------------------------------------------
			// fixed header for mobile
			// --------------------------------------------
			if ($("#header").outerWidth()<691) {
				$("#header").addClass('mobileFixed'); 
				$("#mainContainer").addClass("mobileFixing"); 
			}; 

			// --------------------------------------------
			// activar animaciones
			// --------------------------------------------
			new WOW().init();

		</script>
</div>
