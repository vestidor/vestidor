<?php $this->layout('layouts::default') ?>

<div class="container">
        <h3 class="page-title">Our Blog</h3>
        <div class="blog-grid butique-masonry">
            <div class="masonry-grid" data-layoutmode="masonry" data-cols="3">
                <div class="grid-item masonry-item">         
                    <div class="blog-item">  
                        <div class="post-thumbnail">                               
                            <a class="banner-opacity" href="#"><img alt="17_blog" src="<?php echo dist_url('vertidor/images/blogs/17-Blog1.png'); ?>"></a>
                        </div> 
                        <h3 class="blog-title"><a href="#">Onteger lectus urna ultricies vel lectus</a></h3>
                        <div class="entry-meta">
                            <span class="post-date">20 Dec 2015</span>
                            <span class="blog-comment"><i class="fa fa-comment"></i><span class="count-comment">36</span></span>                                   
                        </div>
                        <div class="blog-short-desc">
                           <p>Nulla laoreet ipsum dignissim magna maximus, vitae euis mod turpis iaculis. Sed pharetra lacus sit amet dui conse quat dignissim bibendum ullamcorper sem.</p> 
                        </div>
                        <a class="readmore" href="blogpost.html" >Readmore</a>
                    </div>                                                            
                </div>
                <div class="grid-item masonry-item">         
                    <div class="blog-item">  
                        <div class="post-thumbnail">                               
                            <a class="banner-opacity" href="#"><img alt="17_blog" src="<?php echo dist_url('vertidor/images/blogs/17-Blog2.png'); ?>"></a>
                        </div> 
                        <h3 class="blog-title"><a href="#">Onteger lectus urna ultricies vel lectus</a></h3>
                        <div class="entry-meta">
                            <span class="post-date">20 Dec 2015</span>
                            <span class="blog-comment"><i class="fa fa-comment"></i><span class="count-comment">36</span></span>                                   
                        </div>
                        <div class="blog-short-desc">
                           <p>Sed a sapien in tellus fringilla vestibulum. Sed elementum nisl eget turpis pharetra, vel posuere felis volutpat. Sed elementum enim nulla, ac molestie orci sollicitudin ac. Cras vitae purus lacus. Pellentesque vel urna id nibh vehicula sagittis eget in turpis.</p> 
                        </div>
                        <a class="readmore" href="blogpost.html" >Readmore</a>
                    </div>                                                            
                </div>
                <div class="grid-item masonry-item">         
                    <div class="blog-item">  
                        <div class="post-thumbnail">                               
                            <a  class="banner-opacity"  href="#"><img alt="17_blog" src="<?php echo dist_url('vertidor/images/blogs/17-Blog3.png'); ?>"></a>
                        </div> 
                        <h3 class="blog-title"><a href="#">Onteger lectus urna ultricies vel lectus</a></h3>
                        <div class="entry-meta">
                            <span class="post-date">20 Dec 2015</span>
                            <span class="blog-comment"><i class="fa fa-comment"></i><span class="count-comment">36</span></span>                                   
                        </div>
                        <div class="blog-short-desc">
                           <p>Nulla laoreet ipsum dignissim magna maximus, vitae euis mod turpis iaculis. Sed pharetra lacus sit amet dui conse quat dignissim bibendum ullamcorper sem.</p> 
                        </div>
                        <a class="readmore" href="blogpost.html" >Readmore</a>
                    </div>                                                            
                </div>
                <div class="grid-item masonry-item">         
                    <div class="blog-item">  
                        <div class="post-thumbnail">                               
                            <a class="banner-opacity" href="#"><img alt="17_blog" src="<?php echo dist_url('vertidor/images/blogs/17-Blog4.png'); ?>"></a>
                        </div> 
                        <h3 class="blog-title"><a href="#">Onteger lectus urna ultricies vel lectus</a></h3>
                        <div class="entry-meta">
                            <span class="post-date">20 Dec 2015</span>
                            <span class="blog-comment"><i class="fa fa-comment"></i><span class="count-comment">36</span></span>                                   
                        </div>
                        <div class="blog-short-desc">
                           <p>Nulla laoreet ipsum dignissim magna maximus, vitae euis mod turpis iaculis. Sed pharetra lacus sit amet dui conse quat dignissim bibendum ullamcorper sem.</p> 
                        </div>
                        <a class="readmore" href="blogpost.html" >Readmore</a>
                    </div>                                                            
                </div>
                <div class="grid-item masonry-item">         
                    <div class="blog-item">  
                        <div class="post-thumbnail">                               
                            <a class="banner-opacity" href="#"><img alt="17_blog" src="<?php echo dist_url('vertidor/images/blogs/17-Blog5.png'); ?>"></a>
                        </div> 
                        <h3 class="blog-title"><a href="#">Onteger lectus urna ultricies vel lectus</a></h3>
                        <div class="entry-meta">
                            <span class="post-date">20 Dec 2015</span>
                            <span class="blog-comment"><i class="fa fa-comment"></i><span class="count-comment">36</span></span>                                   
                        </div>
                        <div class="blog-short-desc">
                           <p>Sed a sapien in tellus fringilla vestibulum. Sed elementum nisl eget turpis pharetra, vel posuere felis volutpat. Sed elementum enim nulla.</p> 
                        </div>
                        <a class="readmore" href="blogpost.html" >Readmore</a>
                    </div>                                                            
                </div>
                <div class="grid-item masonry-item">         
                    <div class="blog-item">  
                        <div class="post-thumbnail">                               
                            <a class="banner-opacity" href="#"><img alt="17_blog" src="<?php echo dist_url('vertidor/images/blogs/17-Blog6.png'); ?>"></a>
                        </div> 
                        <h3 class="blog-title"><a href="#">Onteger lectus urna ultricies vel lectus</a></h3>
                        <div class="entry-meta">
                            <span class="post-date">20 Dec 2015</span>
                            <span class="blog-comment"><i class="fa fa-comment"></i><span class="count-comment">36</span></span>                                   
                        </div>
                        <div class="blog-short-desc">
                           <p>Sed a sapien in tellus fringilla vestibulum. Sed elementum nisl eget turpis pharetra, vel posuere felis volutpat. Sed elementum enim nulla, ac molestie orci sollicitudin ac. Cras vitae purus lacus. Pellentesque vel urna id nibh vehicula sagittis eget in turpis.</p> 
                        </div>
                        <a class="readmore" href="blogpost.html" >Readmore</a>
                    </div>                                                            
                </div>
                <div class="grid-item masonry-item">         
                    <div class="blog-item">   
                        <div class="post-thumbnail">                               
                            <a class="banner-opacity" href="#"><img alt="17_blog" src="<?php echo dist_url('vertidor/images/blogs/17-Blog7.png'); ?>"></a>
                        </div> 
                        <h3 class="blog-title"><a href="#">Onteger lectus urna ultricies vel lectus</a></h3>
                        <div class="entry-meta">
                            <span class="post-date">20 Dec 2015</span>
                            <span class="blog-comment"><i class="fa fa-comment"></i><span class="count-comment">36</span></span>                                   
                        </div>
                        <div class="blog-short-desc">
                           <p>Nulla laoreet ipsum dignissim magna maximus, vitae euis mod turpis iaculis. Sed pharetra lacus sit amet dui conse quat dignissim bibendum ullamcorper sem.</p> 
                        </div>
                        <a class="readmore" href="blogpost.html" >Readmore</a>
                    </div>                                                            
                </div>
                <div class="grid-item masonry-item">         
                    <div class="blog-item">  
                        <div class="post-thumbnail">                               
                            <a class="banner-opacity" href="#"><img alt="17_blog" src="<?php echo dist_url('vertidor/images/blogs/17-Blog8.png'); ?>"></a>
                        </div> 
                        <h3 class="blog-title"><a href="#">Onteger lectus urna ultricies vel lectus</a></h3>
                        <div class="entry-meta">
                            <span class="post-date">20 Dec 2015</span>
                            <span class="blog-comment"><i class="fa fa-comment"></i><span class="count-comment">36</span></span>                                   
                        </div>
                        <div class="blog-short-desc">
                           <p>Sed a sapien in tellus fringilla vestibulum. Sed elementum nisl eget turpis pharetra, vel posuere felis volutpat. Sed elementum enim nulla, ac molestie orci sollicitudin ac. Cras vitae purus lacus. Pellentesque vel urna id nibh vehicula sagittis eget in turpis.</p> 
                        </div>
                        <a class="readmore" href="blogpost.html" >Readmore</a>
                    </div>                                                            
                </div>
                <div class="grid-item masonry-item">         
                    <div class="blog-item">  
                        <div class="post-thumbnail">                               
                            <a class="banner-opacity" href="#"><img alt="17_blog" src="<?php echo dist_url('vertidor/images/blogs/17-Blog9.png'); ?>"></a>
                        </div> 
                        <h3 class="blog-title"><a href="#">Onteger lectus urna ultricies vel lectus</a></h3>
                        <div class="entry-meta">
                            <span class="post-date">20 Dec 2015</span>
                            <span class="blog-comment"><i class="fa fa-comment"></i><span class="count-comment">36</span></span>                                   
                        </div>
                        <div class="blog-short-desc">
                           <p>Nulla laoreet ipsum dignissim magna maximus, vitae euis mod turpis iaculis. Sed pharetra lacus sit amet dui conse quat dignissim bibendum ullamcorper sem.</p> 
                        </div>
                        <a class="readmore" href="blogpost.html" >Readmore</a>
                    </div>                                                            
                </div>
            </div>
        </div>
          
    </div>