$(document).ready(function () {
    $.noConflict();
    if($.cookie("authorized") && $.cookie("authorized")=="true") {
        $("#logOffIndex").show();
    }
    $("#logOffIndex").on("click", function () {
        $.cookie("authorized", false);
    });
});
