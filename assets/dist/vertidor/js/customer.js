$(document).ready(function () {
    $.noConflict();
    if(!$.cookie("authorized") || $.cookie("authorized")=="false") {
        $("#centerMain").load("customerLogin.html");
    }
    else {
        $("#logOffCustomer").show();
        $("#centerMain").load("customerHome.html");
    }
    $("#logOffCustomer").on("click", function () {
        $.cookie("authorized", false);
    });
});
