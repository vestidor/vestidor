$(document).ready(function () {
    $("#frmCustomerLogon").on("submit", function () {
        $.cookie("authorized", true);
    });
    $("#btnManualRegister").on("click", function () {
        $.cookie("authorized", false);
        $("#centerMain").load("customerRegister1.html");
        return false;
    });
});
